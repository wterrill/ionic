for an markdown cheat sheet, click this link:
[cheatsheet](https://bitbucket.org/tutorials/markdowndemo)

# Things remaining for version 1.0 #


+ ~~new issues race condition for requestor.
    **Leif**~~
* ~~finish approvals.
    **Will**~~
* ~~show pending/confirmed requests for renter.
    **Will**~~
* ~~show pending/confirmed offers for the owner.
    **Will**~~
* ~~form validation for registration.
    **Will**~~
* clear or defer issue tracking.
    **BOTH**
* ~~deployment for ios.~~
    **Will**
* ~~deployment for android.~~
    **Leif**
    *~~ Look at straight Play Store ($25) -- other methods seem hard to manage~~
* account management update all info except username.
    **Leif**
* ~~lost password recovery.
    **Leif**~~
* debug information in database.
    **Will**
* ~~reservations table in database.
    **Leif**~~
* ~~owner cancellation of requester.~~
    **Leif**
* ~~Figure out the parking icon on the landing page
    **Leif**~~
* ~~Add badges
    **Will**~~
* ~~ENSURE THAT THE PERSON HAS REGISTERED before telling them that the account was created.
    **Will**~~
* ~~Add "vehicle type: sedan, SUV, etc"  to registration screen
    **Will**~~
## things for Version 2: ##

* use encryption/closed database
* rental reminders: 15 minutes remain
* database cleanup
* ionic 3.1. auth in firebase
* automated backend