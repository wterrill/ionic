// Example requires the use of the `request` library
// https://www.npmjs.com/package/request

var request = require("request");
var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyODc0ZWI2Yy0xNWIyLTRhMDgtODg5Ni0yNzAyY2I5ODQxNzYifQ.d6pZNB2-5GyDOl9ke3HUmPreA6DH06EZp7ni8_g6fNs';

var options = {
  method: 'GET',
  url: 'https://api.ionic.io/auth/test',
  headers: {
    'Authorization': 'Bearer ' + token,
    'Content-Type': 'application/json'
  }
};

request(options, function(err, response, body) {
  if (err) throw new Error(err);
  console.log(body);
});