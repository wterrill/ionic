var mysql = require('mysql');
var prefs = require('./prefs.js');
var connection;

module.exports.connectToHost = function(){
	connection = mysql.createConnection({
		host : prefs.host,
		user : prefs.user,
		password : prefs.password,
		database : prefs.database
	});
};

module.exports.disconnect = function () {
	connection.end(function (err){
		console.log(err.code);
		console.log(err.fatal);
	});
}



// QUERIES ALL ENTRIES FROM A SPECIFIED TABLE

module.exports.queryData = function (table) {
		connection.query('SELECT * FROM ' + table, function(err, rows, fields){
			if (err) throw err;
			
			console.log(rows[0]);
		});
}

// Takes data as input and creates new database
// entries.  If the primary key already exists
// throws an err, entry exists.
module.exports.createData = function(data, table) {
	
}


// Takes data as input and replaces the existing
// database entry with
module.exports.replaceData = function(data, table) {
	
	
}


// Takes data as input and updates the existing
// entry in the table to match the data passed
// search is by primary key
module.exports.updateData = function(data, table) {
	
	
}


// Finds data in the specified table and deletes the
// entry.
module.exports.deleteData = function(data, table) {
	
	
}



