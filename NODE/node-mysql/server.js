
var http = require('http');
var path = require("path");
var url = require("url");
var qs = require("querystring");
var fs = require('fs');
var crud = require('./my_sql_crud.js');


var serverPort = 8080;
var index = 'index.html';

http.createServer(function( request, response) {
	
	// defines what to do with the get method				
	if (request.method === "GET"){
		
		// on the method get we are reading a file index.html
		// we will write the HTML response 200, notify the browser
		// we are sending text/html, and send it the appropriate 
		// data (which is the output from fs.readFile)
		fs.readFile(index, function (err, data){
			response.writeHead(200, {'Content-Type' : 'text/html', 'Content-Length': data.length});
			response.write(data);
			response.end();
		});

	}
	
	
	// defines post request
	else if (request.method === "POST"){
		
		// check the post url -- this allows the server to handle multiple post requests
		if (request.url === "/post"){
			
			// grab the data from the request body.  The length checked
			// is 1 x 10^7 bytes.  If data is larger than 10 MB we kill
			// the request.
			var requestBody = '';
			request.on('data', function(data) {
				requestBody += data;
				if (requestBody.length > 1e7){
					response.writeHead(413, 'Request Entity Too Large', {'Content-Type': 'text/html'});
					response.end('<!doctype html><html><head><title>413</title></head><body>413: Request Entity Too Large</body></html>')
				}
			});
			
			// when request transmission is finished, handle data
			//
			request.on('end', function() {
				console.log(JSON.parse(requestBody));
				console.log("POST SUCCESS!");
				
				console.log("attempt connection to mySQL");
				crud.connectToHost();
				crud.queryData('people');
				
			});
		}
		
	}
}).listen(8080);

console.log("Server running on port" + serverPort);