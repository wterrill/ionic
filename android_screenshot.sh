#!/bin/bash
# Takes a screenshot from connected android phone using ADB
adb shell screencap -p | perl -pe 's/\x0D\x0A/\x0A/g' > ~/Desktop/screenshot.png
