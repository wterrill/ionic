import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { GenericDatabase } from '../../providers/generic-database';
import { DatastreamService } from '../../providers/datastream-service';
import { WindowService } from '../../providers/window-service';
import { NotificationProvider } from '../../providers/notification-provider';

/*
  Generated class for the Request page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-request',
  templateUrl: 'request.html'
})
export class RequestPage {

  private loading: Loading;
  private currentUser: string;
  private debugMode: boolean = true;
  private totalAvailableSpaces: number = 0;
  private invalidSearch: boolean = false;

  private event = {
    startDate: "",
    endDate: '2050-06-05',
    startTime: '07:45',
    endTime: '08:00'
  };

  private currentDate: Date;
  private startDate: Date;
  private endDate: Date;
  private localDate: string;
  private stringDate: string;
  private str_startingDT: string;
  private str_endingDT: string;
  private dat_startingDT: Date;
  private dat_endingDT: Date;
  private startingMsec: number;
  private endingMsec: number;
  private totalMsec: number;  // total number of MSec a user needs a spot.
  private validRequest: Boolean;

  private availableSpotsStream$: {
    spot: String,
    start: Date,
    end: Date,
    length: number,
    status: number
  }[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
    private database: GenericDatabase, private datastream: DatastreamService, private windowService: WindowService,
    private notificationProvider: NotificationProvider) {

    this.currentUser = this.windowService.getKey("user", "");
    this.reset();

    //added by will to fix midnight date error___________________

    this.currentDate = new Date();
    this.localDate = this.currentDate.toLocaleString();

    this.startDate = new Date (this.currentDate.getTime());
    this.startDate.setHours(this.currentDate.getHours() + 1);
    this.startingMsec = this.startDate.getTime();

    this.event.startDate = this.startDate.getFullYear() + "-" +
      ("0" + (this.startDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.startDate.getDate())).slice(-2);
    
    this.endDate = new Date (this.currentDate.getTime());
    this.endDate.setHours(this.currentDate.getHours() + 2);
    this.endingMsec = this.endDate.getTime();

    if(this.debugMode) {
      console.log("currentDate");
      console.log(this.currentDate);
      console.log("startDate");
      console.log(this.startDate);
      console.log("endDate");
      console.log(this.endDate);
    }

    this.event.endDate = this.endDate.getFullYear() + "-" +
      ("0" + (this.endDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.endDate.getDate())).slice(-2);

    this.event.startTime = ("0" + this.startDate.getHours()).slice(-2) + ":00";
    this.event.endTime = ("0" + this.endDate.getHours()).slice(-2) + ":00";

    //end of what Will added, begining of what Will removed.________________________
    // this.currentDate = new Date();
    // this.localDate = this.currentDate.toLocaleString();
    // this.event.startDate = this.currentDate.getFullYear() + "-" +
    //   ("0" + (this.currentDate.getMonth() + 1)).slice(-2) + "-" +
    //   ("0" + (this.currentDate.getDate())).slice(-2);
    // this.event.endDate = this.event.startDate;
    // this.event.startTime = ("0" + (this.currentDate.getHours() + 1)).slice(-2) + ":00";
    // this.event.endTime = ("0" + (this.currentDate.getHours() + 2)).slice(-2) + ":00";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestPage');
  }



  /** #########################################################
   * ###########################################################
   *              !!!!BEGIN QUERY FUNCTION!!!!
   *  #########################################################
   * ###########################################################
   * 
   * 
   * 
   * This function checks the current request and returns those
   * available spaces that match the desired start / end times.
   * 
   * 
   * If the user entered an invalid request, then no data is returned
   * and the user is prompted to update their selection.
   * 
   */
  querySpaces() {

    this.showLoading("Searching available spots...");
    //alert("I am going to try to find your space!");

    var totalAvailableSpots = 0;

    var availableSpotAggregate: any[] = new Array();

    var availableSpotsRefined: {
      spot: String,
      start: Date,
      end: Date,
      length: number,
      status: number,
      owner: String,
      startMsec: number,
      endMsec: number
    }[] = new Array();



    this.availableSpotsStream$ = new Array();

    // set the start date / end date to MSEC format.
    this.checkValues();

    if (this.validRequest) {
      this.datastream.offeredSpotsStream$.map(response => {

        if (this.debugMode) {
          console.log("This is the datastream provided observable: ");
          console.log(response);
          console.log(response.length);
        }

        /* First I try to remove all the spots from my stream that 
        do not have any offers.  If offers is not undefined then I
        push the entire response to a holding variable, aggregate.*/

        for (var i = 0; i < response.length; i++) {
          if (response[i].offers != undefined) {
            availableSpotAggregate.push(response[i]);
          }
        }

        if (this.debugMode) {
          console.log("Available Aggregate.... ");
          console.log(availableSpotAggregate);
        }

        /* Now that I have the available spots (I mean, truly available) I am
        going to start sliding through them to create a new array.  Each entry
        in the array will keep track of the spot number, start, end, length, 
        and finally the status of the spot. 
        
        
        My big fear is that this going to be difficult to "write" or adjust the
        status back up, becaue of the level of mapping I am doing just to get to
        the point where I can create a list to show the user.
  
        I almost wonder if we are not better of doing uniquely qualified names
        that will be easier to "update" when the appropriate time comes.
  
        Actually I think I will be ok.  I think when writing up I just need to 
        write to spot_#/offers/offer_start
  
        Forget my above rant.  I am ok.
        */

        for (var i = 0; i < availableSpotAggregate.length; i++) {

          var tempOfferArray = Object.keys(availableSpotAggregate[i].offers).map(key => availableSpotAggregate[i].offers[key]);

          if (this.debugMode) {
            console.log("The temp offer Array: ");
            console.log(tempOfferArray);
            console.log("Temp offer length: " + tempOfferArray.length);
          }

          if (tempOfferArray.length > 0) {
            for (var j = 0; j < tempOfferArray.length; j++) {
              if (this.debugMode) {
                console.log("start and stop values for loop");
                console.log(tempOfferArray[j].start);
                console.log(this.startingMsec);
                console.log(tempOfferArray[j].end);
                console.log(this.endingMsec);

              }
              if (tempOfferArray[j].start <= this.startingMsec 
              && tempOfferArray[j].end >= this.endingMsec 
              && tempOfferArray[j].offer_status == 1) {

                if (this.debugMode) {
                  console.log("Pushing tempOfferArray[" + j + "]");
                  console.log(tempOfferArray[j]);
                }

                totalAvailableSpots++;
                availableSpotsRefined.push({
                  spot: availableSpotAggregate[i].$key,
                  start: new Date(tempOfferArray[j].start),
                  end: new Date(tempOfferArray[j].end),
                  length: tempOfferArray[j].weight,
                  status: tempOfferArray[j].offer_status,
                  owner: availableSpotAggregate[i].owneruser,
                  startMsec: tempOfferArray[j].start,
                  endMsec: tempOfferArray[j].end
                });
              }
            }
          } else {
            if (tempOfferArray[0].start <= this.startingMsec && tempOfferArray[0].end >= this.endingMsec) {

              if (this.debugMode) {
                console.log("Pushing tempOfferArray[0]");
                console.log(tempOfferArray);
              }

              totalAvailableSpots++;
              availableSpotsRefined.push({
                spot: availableSpotAggregate[i].$key,
                start: new Date(tempOfferArray[0].start),
                end: new Date(tempOfferArray[0].end),
                length: tempOfferArray[0].weight,
                status: tempOfferArray[0].offer_status,
                owner: availableSpotAggregate[i].owneruser,
                startMsec: tempOfferArray[0].start,
                endMsec: tempOfferArray[0].end
              });
            }
          }
        }

        if (this.debugMode) {
          console.log("Refined Available Spots: ");
          console.log(availableSpotsRefined)
        }

        return availableSpotsRefined;

      }).subscribe(filteredSpots => {

        if (this.debugMode) {
          console.log("The spots after mapping the dataStream are: ");
          console.log(filteredSpots);
        }
        setTimeout(() => {
          this.loading.dismiss();
          this.totalAvailableSpaces = totalAvailableSpots;
          this.availableSpotsStream$ = filteredSpots;

          if (this.totalAvailableSpaces < 1) {
            this.invalidSearch = true;
          }

        }, 500);
      }, err => {
        console.log("Error with the offers data stream");
        console.log(err);
      })

    } else {
      alert("Invalid request parameters.  Please update and try again");
    }
  }

  /** #########################################################
   * ###########################################################
   *              !!!!BEGIN CHEKC FUNCTION!!!!
   *  #########################################################
   * ###########################################################
   * 
   * This function checks the current request and turns it into
   * Milliseconds (I think).  It will update a private property
   * validRequest if the entry is invalid.
   * 
   */

  checkValues() {
    // First sunday in November until second Sunday in March -> CST = -06:00
    // Second Sunday in March until first Sunday in November -> CDT = -05:00
    this.str_startingDT = this.event.startDate + "T" + this.event.startTime + ":00-05:00";
    this.str_endingDT = this.event.endDate + "T" + this.event.endTime + ":00-05:00";
    this.dat_startingDT = new Date(this.str_startingDT);
    this.dat_endingDT = new Date(this.str_endingDT);
    this.startingMsec = this.dat_startingDT.getTime();
    this.endingMsec = this.dat_endingDT.getTime();


    // Checking to see that the request start time is not the same
    // or after the request end time (which is an invalid entry)
    // if it is, we set validRequest to false.  
    if (this.startingMsec >= this.endingMsec) {
      this.validRequest = false;
    } else {
      this.validRequest = true;
    }
  }


  /**
   * Request a spot and remove it from status 1 (available) to 2 (pending).  The
   * owner must approve or deny (inside the offer.ts area).  It will handle further
   * status updates.
   * 
   * @param spot The spot the user is trying to request.  It will contain the pertinent information
   * (owner, times, spot number, etc.) to properly update the database.
   */
  request(spot: any): void {
    if (this.debugMode) {
      console.log("trying to request spot: ");
      console.log(spot);
      console.log("offer_" + spot.startMsec);
    }
    this.showLoading("Building your request...");
    // load up the notification
    let start = new Date(this.startingMsec);
    let end = new Date(this.endingMsec);
    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let timeoptions = {hour: "numeric", minute: "numeric"};
    var notificationMessage = this.currentUser.split('@')[0] + " made a park request! \n" 
      + start.toLocaleDateString('en-US', options) + " at " + start.toLocaleTimeString('en-US', timeoptions)
      + "\nTo: \n"
      + end.toLocaleDateString('en-US', options) + " at " + end.toLocaleTimeString('en-US', timeoptions) 
    // update the offer in the database
    this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.startMsec, "offer_status", 2);
    this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.startMsec, "request_user", this.currentUser);
    this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.startMsec, "request_start", this.startingMsec);
    this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.startMsec, "request_end", this.endingMsec);
    this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.startMsec, "request_weight", this.endingMsec - this.startingMsec);

    this.notificationProvider.sendNotification(spot.owner, notificationMessage);

    setTimeout(() => {
      this.loading.dismiss();
    }, 250);

    this.sleep(2000).then(() => {
      this.querySpaces();
    })

  }


  /**
   * Nothing here....
   */
  showLoading(message: string): void {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.present();
  }


  /**
   * Resets the number of total available spaces to adjust
   * the HTML view of the Request page.
   */
  reset(): void {
    this.totalAvailableSpaces = 0;
    this.invalidSearch = false;
  }

  sleep(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

}
