import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GenericDatabase } from '../../providers/generic-database';
import { WindowService } from '../../providers/window-service';
import { DatastreamService } from '../../providers/datastream-service';

/*
  Generated class for the Offer page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html'
})
export class OfferPage {

  public event = {
    startDate: "",
    endDate: '2050-06-05',
    startTime: '07:45',
    endTime: '08:00'
  }

  private availableSpotsStream$: {
    spot: String,
    start: Date,
    end: Date,
    length: number,
    status: number
    }[];

  currentDate: Date;
  startDate: Date;
  endDate: Date;
  localDate: string;
  stringDate: string;
  str_startingDT: string;
  str_endingDT: string;
  dat_startingDT: Date;
  dat_endingDT: Date;
  startingMsec: number;
  endingMsec: number;
  str_timeStartEntered: string;
  str_timeEndEntered: string;
  temp1: any; //for debugging
  temp2: any; //for debugging

  private debugMode: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public gdb: GenericDatabase,
    public windowService: WindowService, public datastream: DatastreamService) {

    this.currentDate = new Date();
    this.localDate = this.currentDate.toLocaleString();

    this.startDate = new Date (this.currentDate.getTime());
    this.startDate.setHours(this.currentDate.getHours() + 1);

    this.event.startDate = this.startDate.getFullYear() + "-" +
      ("0" + (this.startDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.startDate.getDate())).slice(-2);
    
    this.endDate = new Date (this.currentDate.getTime());
    this.endDate.setHours(this.currentDate.getHours() + 2);

    if(this.debugMode) {
      console.log("currentDate");
      console.log(this.currentDate);
      console.log("startDate");
      console.log(this.startDate);
      console.log("endDate");
      console.log(this.endDate);
    }

    this.event.endDate = this.endDate.getFullYear() + "-" +
      ("0" + (this.endDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.endDate.getDate())).slice(-2);

    this.event.startTime = ("0" + this.startDate.getHours()).slice(-2) + ":00";
    this.event.endTime = ("0" + this.endDate.getHours()).slice(-2) + ":00";

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferPage');
  }

  checkvalues() {
    let hoursoffset = this.currentDate.getTimezoneOffset()/60;
    let str_hoursoffset = ("0" + Math.abs(hoursoffset)).slice(-2) + ":00";
    let str_houroffsetSign = "-";
    //hoursoffset = time in minutes between GMT and localetime = GMT - Localtime.  
    //Therefore if offset is positive, you subtract hours to get local timezone.
    //If the sign is negative. localtime is ahead of GMT.
    if (hoursoffset < 0){
      str_houroffsetSign = "+";
    }
    this.str_startingDT = this.event.startDate + "T" + this.event.startTime + ":00" + str_houroffsetSign + str_hoursoffset;
    this.str_endingDT = this.event.endDate + "T" + this.event.endTime + ":00" + str_houroffsetSign + str_hoursoffset;
    this.dat_startingDT = new Date(this.str_startingDT);
    this.dat_endingDT = new Date(this.str_endingDT);
    this.startingMsec = this.dat_startingDT.getTime();
    this.endingMsec = this.dat_endingDT.getTime();
    this.str_timeStartEntered = this.event.startDate + " " + this.event.startTime
    this.str_timeEndEntered = this.event.endDate + " " + this.event.endTime

    if(this.debugMode){
      console.log("timezone offset is: ",this.currentDate.getTimezoneOffset);
      console.log("str_startingDT, str_endingDT:");
      console.log(this.str_startingDT);
      console.log(this.str_endingDT);
    }
  }

  redo() {
    this.str_startingDT = "";
  }

  send() {
    let data: any;

    let currentDate = new Date()

    data = {
      ["offer_" + this.startingMsec]:
      {
        _1start_debug: this.str_timeStartEntered,
        _2end_debug: this.str_timeEndEntered,
        start: this.startingMsec,
        end: this.endingMsec,
        weight: this.endingMsec - this.startingMsec,
        offer_made: currentDate.getTime(),
        offer_status: 1   //status = availability... 1 = available, 0 = not available, 3 = blocked
      }
    }
    this.gdb.createEntry("spots", "spot_" + this.windowService.getKey('spot', '') + "/offers", data);
  }

  queryownspots() {
    
    let totalAvailableSpots = 0;
    let availableSpotAggregate: any[] = new Array();
    let availableSpotsRefined:
      {
        offerKey: String,
        spot: String,
        start: Date,
        end: Date,
        length: number,
        status: number
      }[] = new Array();

    this.availableSpotsStream$ = new Array();


    this.datastream.offeredSpotsStream$.map(response => {
      console.log("This is the datastream provided observable: ");
      console.log(response);
      console.log(response.length);

      /* First I try to remove all the spots from my stream that 
      do not have any offers.  If offers is not undefined then I
      push the entire response to a holding variable, aggregate.*/

      for (var i = 0; i < response.length; i++) {
        if (response[i].offers != undefined && response[i].owneruser == this.windowService.getKey('user', '')) {
          availableSpotAggregate.push(response[i]);
        }
      }

      console.log("Available Aggregate.... ");
      console.log(availableSpotAggregate);


      for (var i = 0; i < availableSpotAggregate.length; i++) {

        var tempOfferArray = Object.keys(availableSpotAggregate[i].offers).map(key => availableSpotAggregate[i].offers[key]);
        console.log("tempOfferArray");
        console.log(tempOfferArray);

        if (tempOfferArray.length > 0) {
          for (var j = 0; j < tempOfferArray.length; j++) {
            let parentKey: any = Object.keys(availableSpotAggregate[i].offers)[j];
            if(this.debugMode){
              console.log("parentKey")
              console.log(parentKey);
            }

            availableSpotsRefined.push({
              offerKey: parentKey, 
              spot: availableSpotAggregate[i].$key,
              start: new Date(tempOfferArray[j].start),
              end: new Date(tempOfferArray[j].end),
              length: tempOfferArray[j].weight,
              status: tempOfferArray[j].offer_status
            });

          }
        } else {
          if (tempOfferArray[0].start <= this.startingMsec && tempOfferArray[0].end >= this.endingMsec) {
            let parentKey: any = Object.keys(availableSpotAggregate[i].offers)[j].toString();
            availableSpotsRefined.push({
              offerKey: parentKey,
              spot: availableSpotAggregate[i].$key,
              start: new Date(tempOfferArray[0].start),
              end: new Date(tempOfferArray[0].end),
              length: tempOfferArray[0].weight,
              status: tempOfferArray[0].offer_status
            });
          }
        }
      }
      console.log("Refined Offered Spots: ");
      console.log(availableSpotsRefined)

      return availableSpotsRefined;

    }).subscribe(filteredSpots => {
      console.log("The spots after mapping the dataStream are: ");
      console.log(filteredSpots);
      this.availableSpotsStream$ = filteredSpots;
    }, err => {
      console.log("Error with the offers data stream");
      console.log(err);
    })
  }//end of queryownspots

  delete(value){
    this.gdb.deleteRecord("spots/" + value.spot +"/offers/" + value.offerKey);
    if(this.debugMode){
      console.log("delete information")
      console.log(value)
      console.log(this.availableSpotsStream$)
    this.queryownspots();
    }
  }
}
