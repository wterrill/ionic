import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Auth, User, UserDetails, IDetailedError, Push, PushToken } from '@ionic/cloud-angular';
import { WindowService } from '../../providers/window-service';
import { ManagementPage } from '../management/management';
import { OfferPage } from '../offer/offer';
import { RequestPage } from '../request/request';
import { SettingsPage } from '../settings/settings';
import { LoginPage } from '../login/login';
/*
  Generated class for the Landing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class LandingPage {

  username: String;
  private debugMode: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private windowService: WindowService, private auth: Auth) {
    //console.log(navParams);
    //console.log(navParams.data);
    this.username = windowService.window.localStorage.getItem('user');
    this.auth = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  logOut() {
    this.auth.logout();
    this.navCtrl.push(LoginPage);
  }

  goToOffer() {
    this.navCtrl.push(OfferPage);
  }

  goToRequest() {
    this.navCtrl.push(RequestPage);
  }

  goToManagement() {
    this.navCtrl.push(ManagementPage);
  }

  goToSettings() {
    this.navCtrl.push(SettingsPage);
  }

}
