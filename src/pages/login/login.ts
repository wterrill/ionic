import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { MessagingPage } from '../messaging/messaging';
import { Auth, User, UserDetails, IDetailedError, Push, PushToken } from '@ionic/cloud-angular';
import { WindowService } from '../../providers/window-service';
import { ConversationsPage } from '../conversations/conversations'; // MAY NOT BE USED ANYMORE - REMOVE FROM THIS PAGE?
import { LandingPage } from '../landing/landing';
import { Platform } from 'ionic-angular'; //This is used to check if we're running ionic serve or if on a real device
import { GenericDatabase } from '../../providers/generic-database';
import { Observable } from 'rxjs/Observable';
import { DatastreamService } from '../../providers/datastream-service';
import { NotificationProvider } from '../../providers/notification-provider';
// the login, signup and auth-service provider were created using this tutorial: https://devdactic.com/login-ionic-2/ so check it out if you're confused -BLT

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  loading: Loading;
  registerCredentials = { email: '', password: '', rememberMe: false };

  private debugMode: boolean = false;


  constructor(private navCtrl: NavController, private authprovider: AuthService,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController,
    public auth: Auth, public push: Push, public user: User,
    private windowService: WindowService, private platform: Platform,
    public gdb: GenericDatabase, private datastreams: DatastreamService,
    private notificationProvider: NotificationProvider) {

    var remember = windowService.getKey('remember', false);

    if (remember) {
      this.registerCredentials.email = windowService.getKey('user', '');
      this.registerCredentials.password = windowService.getKey('password', '');
      this.registerCredentials.rememberMe = remember;
    }
    if (this.debugMode) {
      console.log(windowService.window);
      console.log(this.registerCredentials);
    }
  }

  public createAccount() {
    // Added this to reset user/password combination if "remember me" is set.
    this.windowService.setKey('user', '');
    this.windowService.setKey('password', '');
    this.windowService.setKey('remember', '');
    this.registerCredentials.email = "";
    this.registerCredentials.password = "";
    this.registerCredentials.rememberMe = false;
    this.navCtrl.push(SignupPage);
  }

  public login() {
    this.updateStorage();
    this.showLoading();
    var ionicServe = false;
    if (!this.platform.is("cordova")) {
      if (this.debugMode) {
        console.log("running ionic serve");
      }
      ionicServe = true;
    }
    else {
      if (this.debugMode) {
        console.log("not running ionic serve");
      }
    }
    var access = false;

    this.auth.login('basic', this.registerCredentials)
      .then((x) => {
        if (this.debugMode) {
          console.log("the auth login returned", x);
        }
        if (!ionicServe) {
          this.push.register()
            .then((t: PushToken) => {
              if (this.debugMode) {
                console.log("pushtoken: ", t);
              }
              return this.push.saveToken(t);
            })
            .then((t: PushToken) => {
              if (this.debugMode) {
                console.log('Token saved:', t.token);
              }
            });

          this.push.rx.notification()
            .subscribe((msg) => {
              alert(msg.title + ': ' + msg.text);
            });

        }
        access = true;
        console.log(x);
        setTimeout(() => {
          this.loading.dismiss();
          console.log(this.auth);
          this.navCtrl.setRoot(LandingPage, this.auth);
        })
      },
      (err) => {
        console.log(err);
        access = false;
        let errors = '';
        if (err.message === 'UNPROCESSABLE ENTITY') errors += 'Email isn\'t valid.<br/>';
        if (err.message === 'UNAUTHORIZED') errors += 'Password is required.<br/>';
        this.showError("Access Denied");
      });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    //let access = (credentials.password === "pass" && credentials.email === "email");
    //this.currentUser = new User_local('Simon', 'saimon@devdactic.com');

    this.windowService.setKey('user', this.registerCredentials.email);
    this.windowService.setKey('password', this.registerCredentials.password);
    this.windowService.setKey('remember', this.registerCredentials.rememberMe);

    //Look up user info and put it into windowService


    this.gdb.queryDB_single_item("users", this.registerCredentials.email.replace(".", "-"));
    this.gdb.queryDB_single_item$.subscribe
      (
      onNext => {
        let reply = onNext;

        if (this.debugMode) {
          console.log("queryDB_Single result: ", reply)
        }

        for (var i = 0; i < onNext.length; i++) {

          if (this.debugMode) {
            console.log(onNext[i].$key, onNext[i].$value);
          }

          this.windowService.setKey(onNext[i].$key, onNext[i].$value);

          if (this.debugMode) {
            console.log("Check out windoservice");
            // THIS ALLOWS US TO PRINT THE WINDOW OBJECT
            // THERE YOU CAN DRILL INTO LOCAL STORAGE
            console.log(this.windowService);
          }
        }
        if (this.debugMode) {
          console.log("the color of the car is:", this.windowService.getKey("color", "Didn't work"));
        }
      },
      onError => {
        console.log("onError: ", onError)
      },
      () => { }
      )
    //Updating the login query item.  This grabs the car color (and all other registration info) to be passed to local storage.

  }

  updateStorage() {

    if (this.registerCredentials.rememberMe) {
      this.windowService.setKey('user', this.registerCredentials.email.toLowerCase());
      this.windowService.setKey('password', this.registerCredentials.password);
      this.windowService.setKey('remember', this.registerCredentials.rememberMe);
    } else {
      this.windowService.setKey('user', '');
      this.windowService.setKey('password', '');
      this.windowService.setKey('remember', '');
    }

    console.log(this.windowService.window);

  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(text) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  goToDevMain() {
    this.navCtrl.push(HomePage);
  }

}