import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { LoginPage } from '../login/login';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  createSuccess = false;
  ionicRegisterCredentials = { email: '', password: '' };
  FbRegisterCredentials = { email: '', password: '', spot: '', unit: '', insurance: '', policy: '', make_model: '', color: '', plate: '', telephone: '' };
  private debugMode: boolean = false;

  constructor(private nav: NavController, private auth: AuthService, private alertCtrl: AlertController, private af: AngularFire) { }

  public register() {
    //register with Ionic
    this.auth.register(this.ionicRegisterCredentials).subscribe(success => {
      if (success) {
        this.createSuccess = true;
        this.showPopup("Success", "Account created.");
      } else {
        this.showPopup("Error", "Problem creating account.");
      }
    },
      error => {
        this.showPopup("Error", error);
      });


    //put user info in the firebase database
    const items = this.af.database.list('/users/');
    this.FbRegisterCredentials.email = this.ionicRegisterCredentials.email;
    this.FbRegisterCredentials.password = this.ionicRegisterCredentials.password;

    items.update(this.FbRegisterCredentials.email.replace(".", "-"), {
      password: this.FbRegisterCredentials.password,
      spot: this.FbRegisterCredentials.spot,
      unit: this.FbRegisterCredentials.unit,
      insurance: this.FbRegisterCredentials.insurance,
      policy: this.FbRegisterCredentials.policy,
      make_model: this.FbRegisterCredentials.make_model,
      color: this.FbRegisterCredentials.color,
      plate: this.FbRegisterCredentials.plate,
      telephone: this.FbRegisterCredentials.telephone
    });

    //put spot info in the firebase database. This is where offers will be shown when they are made by the user.
    const spots = this.af.database.list('/spots/');
    spots.update("spot_" + this.FbRegisterCredentials.spot,
      {
        owneruser: this.FbRegisterCredentials.email,
        global_spot_status: 1,
      });
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.nav.push(LoginPage);  //this.nav.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  }
}














// import { Component } from '@angular/core';
// import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

// //added by Will
// import { MessagePage } from '../messaging/messaging';
// import { Auth, User, UserDetails, IDetailedError, Push, PushToken } from '@ionic/cloud-angular';
// import { Datasave } from '../../providers/datasave';
// import { NgModule } from '@angular/core';
// import { IonicApp, IonicModule } from 'ionic-angular';

// @Component({
//   selector: 'page-signup',
//   templateUrl: 'signup.html'
// })
// export class SignupPage {
//   token: any;
//   beer: any;

//   constructor(public navCtrl: NavController, public navParams: NavParams,
//               public auth: Auth, public user: User, public push: Push, public save: Datasave,
//               public alertCtrl: AlertController, public loadingCtrl:LoadingController) {
//     this.token = this.navParams.get('token');
//     this.beer = this.navParams.get('beer');
//   }

//   ionViewDidLoad() {
//     console.log('ionViewDidLoad SignupPage');
//   }
//   pushuser(){

//     var zmail = 'william.terrill@gmail.com';

//     let details: UserDetails = {
//       'email': zmail, 
//       'password': 'puppies123',
//       'name': 'Will',
//       'username': 'wterrill',
//       'image': 'https://upload.wikimedia.org/wikipedia/commons/7/78/GravityTap.jpg'
//       };

//     this.auth.signup(details).then(() => {
//     // `this.user` is now registered
//     }, (err: IDetailedError<string[]>) => {
//       for (let e of err.details) {
//         if (e === 'conflict_email') {
//           alert('Email already exists.');
//         } else {
//           alert(e);
//         }
//       }
//     });                      
//   }

//   gotosendmessage(){
//     this.navCtrl.push( MessagePage );   
//   }


//   loginuser(){

//       let loader = this.loadingCtrl.create({
//         content: "Logging in..."
//       });
//       loader.present();

//     var ionicServe = true;  //Set this to false when using ionic serve to debug

//     let details = {'email': 'william.terrill@gmail.com', 'password': 'puppies123'};
//       this.auth.login('basic', details).then(( x ) => {
//               if (ionicServe == false) { this.push.register().then((t: PushToken) => {   
//               return this.push.saveToken(t);
//               }).then((t: PushToken) => {
//                 console.log('Token saved:', t.token);
//                 this.save.savetoken(t);
//               });

//               this.push.rx.notification()
//               .subscribe((msg) => {
//                 alert(msg.title + ': ' + msg.text);
//               }); 
//               }
//         console.log('ok i guess? ', x);
//         loader.dismissAll();
//         //this.navCtrl.setRoot(HomePage);        
//       }, (err) => {
//         loader.dismissAll();
//         console.log(err.message);

//         let errors = '';
//         if(err.message === 'UNPROCESSABLE ENTITY') errors += 'Email isn\'t valid.<br/>';
//         if(err.message === 'UNAUTHORIZED') errors += 'Password is required.<br/>';

//         let alert = this.alertCtrl.create({
//           title:'Login Error', 
//           subTitle:errors,
//           buttons:['OK']
//         });
//         alert.present();
//       });

//     if (this.auth.isAuthenticated()) 
//     {
//       alert("authenticated!");
//       //this.user.load('97edf6df-6031-407a-bcc5-5770cc406a24')
//       this.push.saveToken(this.token); 
//       }
//       else
//       {
//         alert("failed authentication");
//       }
//   }
// }

