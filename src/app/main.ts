import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

// added by will for greensock in svgmap
import 'gsap';

platformBrowserDynamic().bootstrapModule(AppModule); 
