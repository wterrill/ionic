import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Database } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';
import { WindowService } from './window-service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2'
/*
  Generated class for the MessagesDatabase provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MessagesDatabase {



  // ************           DANGER LEIF BROCKMAN DANGER **********************
  // https://www.joshmorony.com/building-mobile-apps-with-ionic-2/observables-in-ionic2.html
  // pertains directly too conversationStream (an Observable) and conversationObserver (an Observer)
  messageStream: any;
  messageObserver: any;

  // Generic properties for ConversationsDatabase
  connectionStatus: any;
  messageDB: any;
  private messageList: any;
  static appUserIdentifier = 'ionic_user_9a7f489c';
  public currentUser: any;
  convList: FirebaseListObservable<any[]>;
  chatId: any;
  private debugMode: boolean = false;

  constructor(public http: Http, private db: Database, private windowService: WindowService, private af: AngularFire) {

    this.messageStream = Observable.create(observer => {
      this.messageObserver = observer;
    });

  }

  messageDatabaseConnect(chatId: string) {
    this.convList = this.af.database.list("/messages/" + chatId);
    this.chatId = chatId;
  }

  getMessages(chatId: string, limitAmount: number) {
    this.messageDatabaseConnect(chatId);
    this.currentUser = this.windowService.getKey('user', '');

    var test = true;
    if (test) {
      if (this.currentUser != null) {
        console.log(this.currentUser);

        // find all entries in the current database
        // we can separate this to find all based on chatId 
        // if we want to use a single collection for messages between all users
        // right now using separate collections
        // limiting the results to the first 10 items.


        const queryObservable1$ = this.af.database.list('/messages/' + chatId, {
          query: {
            limitToLast: limitAmount
          }
        });

        this.messageStream = queryObservable1$;

        // queryObservable1$.subscribe(
        //   onNext => { console.log("this is from the conversation list-*-*", onNext)},
        //   onError => { console.log(onError)},
        //   ()=> {}
        //   )

      } else {
        var err = new Error('Error: current user not specified');
        return err;
      }
    } 
  }



  // messageDatabaseDisconnect() {
  //   this.connectionStatus = this.db.status();

  //   if (this.connectionStatus != 'disconnected') {
  //     this.db.disconnect()
  //   }
  // }

  // messageSubscription() {
  //   return this.messageList;
  // }

  sendMessage(message: String) {

      this.currentUser = this.windowService.getKey('user', '');

      this.messageDB.insert({
          fromUser: this.currentUser,
          dateTime: new Date(),
          message: message
      });

  }

  sendMessageFirebase(message: String) {
    var milliseconds = new Date().getTime();
    const items = this.af.database.list("/messages/"); 
    items.update(this.chatId+"/"+milliseconds, {"fromUser":this.currentUser,"message":message});
  }
}
