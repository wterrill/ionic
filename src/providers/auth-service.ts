
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';


import { MessagingPage } from '../pages/messaging/messaging';
import { Auth, User, UserDetails, IDetailedError, Push, PushToken } from '@ionic/cloud-angular';




export class User_local {
  name: string;
  email: string;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}

@Injectable()
export class AuthService {

  private debugMode: boolean = false;

  constructor(public http: Http, public auth: Auth, public push: Push) {
    if (this.debugMode) {
      console.log('Hello AuthService Provider');
    }
  }

  currentUser: User_local;

  public login(credentials) {
    alert("there has to be an easier way....goddammit");
  }

  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // Added by Will to store on the back end

      /////////////////////////////////////////////////////////

      this.auth.signup(credentials).then(() => {
        // `this.user` is now registered
      },
        (err: IDetailedError<string[]>) => {
          for (let e of err.details) {
            if (e === 'conflict_email') {
              alert('Email already exists.');
            } else {
              alert(e);
            }
          }
        });

      //////////////////////////////////////////////////////

      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  public getUserInfo(): User_local {
    return this.currentUser;
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }
}