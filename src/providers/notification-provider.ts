import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers } from '@angular/http';
import { WindowService } from './window-service';

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NotificationProvider {

  private currentUser: String;
  private debugMode: boolean = true;

  constructor(public http: Http, private windowService: WindowService) {

    console.log('Hello NotificationProvider Provider');
    this.currentUser = this.windowService.getKey('user', '');

  }




  /**
   * Helper function for sending a notification / pushing a notification to an end user.
   * Must be supplied the end user and the notification message (e.g. You have 1 new message!).
   * 
   * @param toUser The user the notification needs to be sent to, as a string (CURRENTLY USING EMAIL ADDRESS!)
   * @param notificationMsg The notification the end user should see.
   */

  sendNotification(toUser: String, notificationMsg: String) {

    if (this.debugMode) {
      console.log("Your message for " + toUser + ' is: ' + notificationMsg);
    }

    /// // This section sends the notification via ionic cloud for each message
    var link = "https://api.ionic.io/push/notifications";
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyODc0ZWI2Yy0xNWIyLTRhMDgtODg5Ni0yNzAyY2I5ODQxNzYifQ.d6pZNB2-5GyDOl9ke3HUmPreA6DH06EZp7ni8_g6fNs');

    var payload = {
      //"tokens": "foj0GHATkec:APA91bFYduDuE9DLAkI9pQIrmaieMKutKIopVzWBMDx9lrDe7n7N2xbohv_cK_MtgIoF8TNMU4CeSlhC9CvqvS7gZSOH8b9M27ps49O3UAmH6-60Q7AWQq_wGrWTvIgA1erWfCVSlkva",
      "emails": toUser,
      "profile": "push_notification",
      "notification": {
        "message": notificationMsg
      }
    }

    var jsonString = JSON.stringify(payload);
    this.http.post(link, jsonString, { headers: headers })
      .subscribe(res => {
        console.log(res.json());
      }, (err) => {
        console.log(err);
      });
    console.log(jsonString);
    // end ionic push notifications
  }
}
