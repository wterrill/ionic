import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
//import { Database } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import { WindowService } from './window-service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

declare var firebase: any;

@Injectable()
export class GenericDatabase {
  public existingChatId: string;
  public currentUser: string;
  public queryDB_single_item$: FirebaseListObservable<any[]>;
  public genericQueryStream$: any;
  private tableQueryResultSet: any;
  private debugMode: boolean = true;

  constructor(public http: Http, public af: AngularFire, private windowService: WindowService) {

    console.log("Constructing a new generic database");
    this.currentUser = this.windowService.getKey('user', ''); //gets the logged in user name

  }



  createEntry(table, record, data) {
    // This creates entries in the firebase database. 
    // - table is the string name of the top-level branch of the database (users, messages, etc.) 
    // - record is the string name of the individual record within the table (i.e. username, message number)
    // - data is a structure that holds all of the lower level information, in the form of 
    //   { datafield1: "string1", datafield2: "string2", datafield3: {subdatafield1: "string3", subdatafield2:"string4"} }
    //
    // example: 
    //     data = { user1: "wterrill", user2: "it works", realnames: {user1: "Will", user2:"Leif"}}
    //     this.gdb.createEntry("table","record", data);
    // 
    // would create the following database entry:
    // Neighborly-577cs    <-- Database itself.  Configured elsewhere.
    // |_ table
    //      |_ record
    //           |_ user1: "wterrill"
    //           |_ user2: "lbrockman"
    //           |_ realnames
    //                |_ user1: "Will"
    //                |_ user2: "Leif"
    //
    if (this.debugMode) {
      console.log("inserting into table ", table, " with record: ", record, "with data", data);
    }
    const items2 = this.af.database.list(table);
    items2.update(record, data);
  }

  queryDB_single_item(table: String, item: String) {
    //This creates and observable using the table and item information passed to it, and stores
    // the observable in queryDB_single_item$ to allow the pages to subscribe to it.
    if (this.debugMode) {
      console.log("queryDB_single entry for table ", table, " and item ", item);
    }
    this.queryDB_single_item$ = this.af.database.list("/" + table + "/" + item,
      {
        query:
        {
          //you don't need anything here for single queries
        }
      });
  }

  queryDB_single_table(table: String) {
    if (this.debugMode) {
      console.log("Querying for the table ", + table + ": ");
      console.log(table);
    }
    this.af.database.list("/" + table, {
      query: {

      }
    }).subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.genericQueryStream$ = Observable.create(observer => {
        try {
          observer.next(queryResultSet);
          observer.onCompleted();
        } catch (error) {
          observer.onError(error);
        }
      })
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
  }

  deleteRecord(path: any){
    if (this.debugMode){
      console.log("reached deleteRecord");
      console.log(path);
    }
    const items3 = this.af.database.object(path);
    items3.remove();
  }



  queryDB_table_byChildEqual(table: String, child: any, childValue: any) {
  /**
   * 
   * @param table the table to start the search within
   * @param child the parameter you want to limit results by (e.g. ID, name, address, status, etc.)
   * @param childValue the value you want that parameter to be EQUAL too (e.g. SELECT FROM table WHERE child == childValue)
   */
    if (this.debugMode) {
      console.log("Querying for the table ", + table + ": ");
      console.log(table);
      console.log("and the children " + child + " whose values equal: " + childValue);
    }
    this.af.database.list("/" + table, {
      query: {
        orderByChild: child,
        equalTo: childValue
      }
    }).subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.genericQueryStream$ = Observable.create(observer => {
        try {
          observer.next(queryResultSet);
          observer.onCompleted();
        } catch (error) {
          Observable.throw(error);
        }
      })
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
  }

  update_DB_childByParent(path: String, key: any, childId: any, newChildValue: any): boolean {
  /**
   * Returns true if the update was succesful and false otherwise.
   * 
   * @param path path within the database that data is located (/users or /spots/spot253)
   * @param key The key the child exists on.  If path is /spots/spot253/offers key would be the offer_epoch you wish to update a child on.
   * @param childId The child you want to update.  spots/spot253/offers, key of offer_153, child offer_status would update the offer status property on offer_153
   * @param newChildValue Value you wish to set the child too.
   */
    var success: boolean;
    var db = firebase.database().ref(path).child(key);

    if (this.debugMode) {
      console.log("The database is: ");
      console.log(db);
    }

    if (db != undefined || db != null) {

      if (this.debugMode) {
        console.log("Updating the status!");
      }
      db.update(
        { [childId] : newChildValue }
      )
      success = true;

    } else {

      console.log("Error finding data in database.");
      success = false;

    }

    if (success) {
      return true;
    } else {
      return false;
    }

  }



}
