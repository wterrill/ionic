import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
//import { Database } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/onErrorResumeNext';
import { WindowService } from './window-service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
/*
  Generated class for the DatastreamService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DatastreamService {

  private currentUser: string;

  public offeredSpotsStream$: any;

  private debugMode: boolean = false;

  constructor(public http: Http, private af: AngularFire, private windowService: WindowService) {
    if (this.debugMode) {
      console.log("Hello DatastreamService");
    }
    this.currentUser = this.windowService.getKey('user', ''); //gets the logged in user name

    // establishes the connection to the offers data stream
    this.provideOfferedSpots();



  }

  provideOfferedSpots(): void {
    if (this.debugMode) {
      console.log("Querying for spots whose global_spot_status == 1");
    }
    this.af.database.list("/spots", {
      query: {
        orderByChild: "global_spot_status",
        equalTo: 1
      }
    }).subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.offeredSpotsStream$ = Observable.create(observer => {
        try {
          observer.next(queryResultSet);
          observer.oncompleted();
        } catch (error) {
          Observable.throw(error);
        }
      })
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
  }


}
