import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  https://forum.ionicframework.com/t/store-objects-in-localstorage/44788

  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  A WindowService that allows access to the window.localStorage

  It also wraps in several useful methods.  This uses the strict
  localStorage and is bound by its limitations (such as storage size).
  For larger data / storage needs, see the storage-service provider.

*/
@Injectable()
export class WindowService {

  private debugMode: boolean = false;
  public window = window;
  constructor(public http: Http) {

  }

  setKey(key, value) {
    window.localStorage[key] = value;
  }

  getKey(key, defaultValue) {
    return window.localStorage[key] || defaultValue;
  }

  setObject(key, value) {
    window.localStorage[key] = JSON.stringify(value);
  }

  getObject(key, value) {
    return JSON.parse(window.localStorage[key] || '{}');
  }

  remove(key) {
    window.localStorage.removeItem(key);
  }

}
