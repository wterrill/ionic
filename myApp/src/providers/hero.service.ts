import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Hero } from '../pages/detail/hero';
import { HEROES } from '../pages/detail/mock-heroes';





/*
  Generated class for the HeroService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HeroService {

  constructor(public http: Http) {
    console.log('Hello HeroService Provider');
  }
  getHeroes():  Promise<Hero[]> {
    return Promise.resolve(HEROES);
  }

  getHeroesSlowly(): Promise<Hero[]> {
  return new Promise(resolve => {
    // Simulate server latency with 2 second delay
    setTimeout(() => resolve(this.getHeroes()), 2000);
  });
}

  

}

