import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import {Observable} from 'rxjs/Rx';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
  //template: '<h3>latitude {{latitude}}</h3><h3>longitude {{longitude}}</h3>'
})


export class AboutPage {
  latitude: any;
  longitude: any;
  accuracy: any;
  altitude: any;
  altitudeAccuracy: any;
  heading: any;
  speed: any;

  dlatitude: any;
  dlongitude: any;
  daccuracy: any;
  daltitude: any;
  daltitudeAccuracy: any;
  dheading: any;
  dspeed: any;

  appname : string;
  time : string;
  timecolor : string;
  


      constructor(public navCtrl: NavController) {
        Geolocation.getCurrentPosition().then((resp) => {
          this.latitude = resp.coords.latitude;
          this.longitude = resp.coords.longitude;
          this.accuracy = resp.coords.accuracy;
          this.altitude = resp.coords.altitude;
          this.altitudeAccuracy = resp.coords.altitudeAccuracy;
          this.heading = resp.coords.heading;
          this.speed = resp.coords.heading;
            
          this.appname = "App Name";
          this.time = "02:00";
          this.timecolor = "primary";


        }).catch((error) => {
          console.log('Error getting location', error);
        });

let watch = Geolocation.watchPosition();
watch.subscribe((data) => {
      this.dlatitude = data.coords.latitude;
      this.dlongitude = data.coords.longitude;
      this.daccuracy = data.coords.accuracy;
      this.daltitude = data.coords.altitude;
      this.daltitudeAccuracy = data.coords.altitudeAccuracy;
      this.dheading = data.coords.heading;
      this.dspeed = data.coords.heading;
      
 // data can be a set of coordinates, or an error (if an error occurred).
 // data.coords.latitude
 // data.coords.longitude
});


  }


}









