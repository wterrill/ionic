import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { DetailPage } from '../detail/detail'; 

import {
  Push,
  PushToken,
  CloudSettings
} from '@ionic/cloud-angular';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'com.ionicframework.myapp515619',
  },
  'push': {
    'sender_id': '514979035420',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items: any[];

  constructor(public navCtrl: NavController, public push: Push) {
    this.items = []
    for (let i = 0; i<5; i++)
    {
      this.items.push({
        text: 'Item' + i,
        id: i
      });
    }

    this.push.register().then((t: PushToken) => {
      return this.push.saveToken(t);
    }).then((t: PushToken) => {
      console.log('Token saved:', t.token);
    });

    this.push.rx.notification()
      .subscribe((msg) => {
        alert(msg.title + ': ' + msg.text);
      });


  }
itemSelected(item){
  this.navCtrl.push(DetailPage,{
    item: item
  });

}
}
