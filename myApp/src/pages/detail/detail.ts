import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Hero } from './hero';
import { HeroService } from '../../providers/hero.service'
import { OnInit } from '@angular/core';




/*
  Generated class for the Detail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/




@Component({
  selector: 'page-detail'
  ,
  template: 
  `
  <h1>{{title}}</h1>
  <h2>My Heroes</h2>
  <ul class = "heroes"> 
    <li *ngFor="let hero of heroes" 
    [class.selected]="hero===selectedHero"
    (click)="onSelect(hero)">
      <span class="badge">{{hero.id}}</span> {{hero.name}}
    </li>
  </ul>    
  <my-hero-detail [hero]="selectedHero"></my-hero-detail>
  `
  ,
  styles: [
    `
    .selected { 
      background-color: #CFD8DC !important;
      color: white
      }
    .heroes { 
      margin: 0 0 2em 0;
      list-style-type: none;
      padding: 0;
      width: 15em;
      }
    .heroes {
      cursor: pointer;
      position: relative;
      left: 0;
      background-color: #EEE;
      margin: .5em;
      padding: .3em 0;
      haight: 1.6em;
      border-radius: 4px;
      }
    .heros li.selected:hover {
      background-color: #BBD8DC !important;
      color: white;
      }
    .heroes li:hover {
      color: #607D8B;
      background-color: #DDD;
      left: .1em;
     }
    .heroes .badge {
      display: inline-block;
      font-size: small;
      color: white;
      padding: 0.8em 0.7em 0 0.7em;
      background-color: #607D8B;
      line-height: 1em;
      postition: relative;
      left: -1px;
      top: -4px;
      height: 1.8em;
      margin-right: .8em;
      border-radius: 4px 0 0 4px;
    }
    [class.selected]="hero===selectedHero"
    `
  ],
  providers: [HeroService]
})
export class DetailPage  implements OnInit {
  item: any;
  title = 'Tour of Heroes';
  heroes: Hero[];
  selectedHero: Hero;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private heroService: HeroService) {
    this.item = navParams.get('item')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero; 
  }
  
  getHeroes(): void {
    this.heroService.getHeroesSlowly()
    .then(heroes => this.heroes = heroes);
  }

  ngOnInit(): void {
    this.getHeroes();
  }

}





