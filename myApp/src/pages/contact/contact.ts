import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http, Headers, RequestOptions}             from '@angular/http';
import {Rx} from "@reactivex/rxjs";



@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  data: any;
  
  constructor(public navCtrl: NavController, public http: Http) {


  }

  registerBtn(fName, lName, eMail, invite) {
    
    var link = "http://52.25.18.254/api.php/potluck"
    var d = new Date();    
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyODc0ZWI2Yy0xNWIyLTRhMDgtODg5Ni0yNzAyY2I5ODQxNzYifQ.d6pZNB2-5GyDOl9ke3HUmPreA6DH06EZp7ni8_g6fNs');
    
    
    var registerUser = {
        
        "name": fName,
        //"lName": lName,
        "food": eMail,
        "confirmed": invite,
        "signup_date": d.getDate()
    }
    
    var jsonString = JSON.stringify(registerUser);

    //alert(jsonString);
    this.http.post(link, jsonString, {headers: headers})
       	.subscribe(res => {
	      	console.log(res.json());
	      }, (err) => {
	      	console.log(err);
	      });
    console.log(jsonString);
    alert("sent");


    // Hey Leif... I got the answer for this problem from this page: https://forum.ionicframework.com/t/how-to-create-http-get-request-in-angular2-ionic2/60882

  }

  delBtn(delRec){
    var link = "http://52.25.18.254/api.php/potluck"
    alert(delRec);
    this.http.delete(link, delRec);
       // .subscribe(res => {
	     // 	console.log(res.json());
	     // }, (err) => {
	     // 	console.log(err);
	     // });
  }

}

