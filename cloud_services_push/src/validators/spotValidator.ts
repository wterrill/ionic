import { FormControl } from '@angular/forms';
import { WindowService } from '../providers/window-service';
 
export class SpotValidator {
 
    static isValid(control: FormControl, windowService: WindowService): any {
        console.log("control from SpotValidator");
        console.log(control);

    

        if (control.value == ""){
            return null;
        }

        if (control.parent != null && control.parent.value.building == "" ){
            return {"spot_error_text": "Please select a building first"};
        }

        if (control.parent != null && control.parent.value.building == "metropolitan" &&  control.parent.value.garage =="garage1"){
            return {"spot_error_text": "Please select a garage above"};
        }
        
        if(isNaN(control.value)){
            return {"spot_error_text": "Please enter a number"};
        }
 
        if(control.value % 1 !== 0){
            return {"spot_error_text": "Please enter a whole number"};
        }
 
        if (control.parent != null && control.parent.value.building == "metropolitan"){
 
            if ((control.value < 31 || control.value > 260) && control.value < 10000){ //this allows for test parking spots during validation.
                return {"spot_error_text": "Please enter a spot number between 31 and 260. For testing, greater than 10,000"};
            }
        } 
        
        if (control.parent != null && control.parent.value.building == "1111WMadison"){
            if ((control.value > 25 || control.value <1)  && control.value < 10000){ //this allows for test parking spots during validation.
                return {"spot_error_text": "Please enter a spot number between 1 and 25. For testing, greater than 10,000"};
            }
        }
 
        return null;
    }
 
}