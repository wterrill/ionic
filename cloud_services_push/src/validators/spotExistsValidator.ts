import { FormControl } from '@angular/forms';
import { GenericDatabase } from '../providers/generic-database';
import { Injectable } from '@angular/core';


@Injectable()
export class SpotExistsValidator {
    
    debouncer: any;

    constructor(public database: GenericDatabase) {

    }

    checkSpotExists(control: FormControl): any {

        console.log("Entered checkSpotExists");

        clearTimeout(this.debouncer);

        return new Promise((resolve) => {

            this.debouncer = setTimeout(() => {
                let building = control.parent.value.building;
                let garage = "";
                if (control.parent.value.garage == ""){
                    garage = "garage1";
                } else {
                    garage = control.parent.value.garage;
                }
                console.log("spotexistsValidator")
                console.log(control.value);
                var first_step = control.value.toString(); //convert it to a string just in case it's in the form of a number
                var replaced = first_step.replace(/\./g,","); //make sure there aren't any bad characters

                if (control.value != ""){
                    let temppromise = this.database.queryDB_single_item_as_Promise("/buildings/" + building + "/" + garage + "/spots/" , "spot_" + replaced);
                    console.log(building);
                    console.log(garage);
                    console.log( "control value below");
                    console.log(control);
                    temppromise.then((res) => {
                        //console.log('I get called:', res === 123);
                        console.log(res); 

                        if (res.length < 1 ) {
                            console.log("it's undefined");
                            resolve(null);
                        } else {
                           console.log("it's defined");
                            resolve({"spotExists_error_text": "Sorry, that spot is already in use! We will have multiple accounts tied to a single spot in the future. However, for now, only one spot per user."});
                            //resolve({ 'spotInUse': true });
                        }

                    });
                } else {
                    resolve(null);
                }

            }, 2000);

        });
    }

}

