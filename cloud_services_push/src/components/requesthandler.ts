
import { GenericDatabase } from "../providers/generic-database";
import { OfferHandler } from './offerhandler';
import { Transaction } from './library';
import { AlertController } from "ionic-angular";
import { AuthService } from '../providers/auth-service';
import { User_local as User } from '../providers/auth-service';
import * as moment from 'moment';

export class RequestHandler {

    private debugMode: boolean = true;
    private userDetails: User;

    constructor(private database: GenericDatabase, private auth: AuthService, private alertCtrl: AlertController) {

        if (this.debugMode) {
            console.log('Building a new RequestHandler!');
        }

        this.userDetails = auth.getUserInfo();
    }

    request(spot, startingMsec, endingMsec): Promise<any> {
        if (this.debugMode) {
            console.log('RequestHandler is attemping to request a the spot "', spot, '"');
        }

        return new Promise((resolve, reject) => {
            this.requestCheck(spot).then(() => {

                spot._3_request_start = moment.utc(startingMsec).local().toString();
                spot._4_request_end = moment.utc(endingMsec).local().toString();
                spot.offer_status = 2;
                spot.request_user = this.userDetails.email;
                spot.request_start = startingMsec;
                spot.request_end = endingMsec;
                spot.request_weight = endingMsec - startingMsec;

                this.additionalOfferCheck(spot).then(modifiedOffer => {

                    var offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);
                    offerHandler.updateOffer(modifiedOffer).then(() => {
                        resolve();
                    }).catch(err => {
                        if (this.debugMode) {
                            console.log('Updating the offer from request handler line 45 failed.  Err was: ', err);
                            reject();
                        }
                    })
                })
            }).catch(() => {
                reject('The offer requested for rental was no longer available -- reject');
            })
        })
    }


    /**Looks up a potential spot a user is requesting in the databse
     * to ensure that it is still a valid offer.  If the query result
     * offer_status does not match the passed spots offer_status the
     * promise will reject.  Handle the reject with the .catch() method
     * in the calling function.
     * 
     * @param spot the spot that must be checked for availability
     * @return Returns a promise that can be used to ensure the validity
     * of the data check.  Will resolve() when statuses are equal, and
     * will reject on any other condition.
     */
    requestCheck(spot): Promise<any> {

        if (this.debugMode) {
            console.log("Checking availability of the spot : ", spot);
        }
        return new Promise((resolve, reject) => {
            var queryResult: {
                offer_status: number
            }[] = new Array();

            this.database.queryDB_single_item_as_Promise('/buildings/' + spot.building + '/' + spot.garage + '/spots/' + spot.spot + '/offers', 'offer_' + spot.offer_start).then(data => {
                if (this.debugMode) {
                    console.log("data after the promise", data);
                }

                if (data.length > 0) {

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].$key === 'offer_status') {
                            queryResult.push({
                                offer_status: data[i].$value
                            });
                            break;
                        }
                    }

                    if (this.debugMode) {
                        console.log('searching for ', spot, 'yielded: ', queryResult);
                    }

                    if (queryResult[0].offer_status === spot.offer_status) {

                        if (this.debugMode) {
                            console.log('the offer availability was confirmed');
                        }

                        resolve();

                    } else {

                        if (this.debugMode) {
                            console.log('the offer was no longer available');
                        }

                        reject();

                    }
                } else {
                    if (this.debugMode) {
                        console.log('no offer was found -- rejecting');
                    }
                    reject();
                }

            });

        });

    }

    /**
     * Checks the requested spot to see if any additional offers
     * can be made (can we splice the request up into multiple offers).
     * 
     * Will always resolve with the spot -- the original spot will be
     * ammended to include the appropriate date for updating into the database.
     * 
     * @param spot The spot the renter is trying to request from the owner.
     * 
     */

    additionalOfferCheck(spot): Promise<any> {

        return new Promise((resolve) => {

            if (this.debugMode) {
                console.log('Checking to see if I can make more offers from your spot...');
            }

            if (((spot.request_end - spot.offer_start) > 3600000) || ((spot.offer_end - spot.request_end) > 3600000)) {

                if (this.debugMode) {
                    console.log('Your spot has more availability!');
                }
                resolve(this.createAdditionalOffer(spot));

            } else {
                if (this.debugMode) {
                    console.log('Nope.  Your spot is the perfect size.');

                }
                resolve(spot);
            }
        });

    }

    private createAdditionalOffer(offer): Transaction {
        var tempOffers = new Array();

        for (var i = 0; i < 2; i++) {

            tempOffers.push(new Transaction(offer._1_offer_start, offer._2_offer_end, offer._3_request_start, offer._4_request_end,
                offer.building, offer.garage,
                offer.offer_end, offer.offer_start, offer.offer_status, offer.offer_weight,
                offer.owner, offer.request_end, offer.request_start, offer.request_user,
                offer.request_weight, offer.spot));
        }

        if ((offer.request_start - offer.offer_start) >= 3600000) {

            tempOffers[0].offer_end = offer.request_start; //- 300000;

            offer.offer_start = offer.request_start;

            if (this.debugMode) {
                console.log('We can add to the front end of the current request / offer: ');
                console.log('Offer Start: ' + offer.offer_start);
                console.log('Offer End: ' + offer.offer_end);
                console.log('TempOffer Start: ' + tempOffers[0].offer_start);
                console.log('TempOffer End: ' + tempOffers[0].offer_end);
            }

            var offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);

            offerHandler.createOffer(tempOffers[0], 1);

        }

        if ((offer.offer_end - offer.request_end) >= 3600000) {

            tempOffers[1].offer_start = offer.request_end; //+ 300000;
            offer.offer_end = offer.request_end;

            if (this.debugMode) {
                console.log('We can add to the BACK end of the current request / offer: ');
                console.log('Offer Start: ' + offer.offer_start);
                console.log('Offer End: ' + offer.offer_end);
                console.log('TempOffer Start: ' + tempOffers[1].offer_start);
                console.log('TempOffer End: ' + tempOffers[1].offer_end);
            }

            var offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);

            offerHandler.createOffer(tempOffers[1], 1);
        }

        if (this.debugMode) {
            console.log("Returning the offer to the calling function: ");
            console.log(offer);
        }
        return offer;
    }
}

