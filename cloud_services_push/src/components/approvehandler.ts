import { GenericDatabase } from "../providers/generic-database";
import { AuthService } from '../providers/auth-service';
import { User_local as User } from '../providers/auth-service';
import { AlertController } from "ionic-angular";
import { OfferHandler } from './offerhandler';
import { ReservationHandler } from './reservationhandler';
import { Transaction } from './library';
//import * as moment from 'moment';


export class ApproveHandler {

    private debugMode: boolean = true;
    private offerHandler: OfferHandler;
    private reservationHandler: ReservationHandler;
    private userDetails: User;

    constructor(private alertCtrl: AlertController, private database: GenericDatabase,
        private auth: AuthService) {

        if (this.debugMode) {
            console.log('Building a new SpotHandler!');
        }

        this.userDetails = auth.getUserInfo();

    }

    /**Works in conjunction with a few private methods to 
     * approve a spot request for an owner. 
     * 
     * @param spot The spot the owner is approving, 
     * more specifically the offer the owner is approving.
     */

    approve(spot): Promise<any> {

        if (this.debugMode) {
            console.log("ApproveHandler: Allowing your spot : ");
            console.log(spot);
            console.log(" to be reserved by user " + spot.request_user + " beginning on " + spot.offer_start);
        }
        return new Promise((resolve, reject) => {
            this.additionalOfferCheck(spot).then(data => {

                if (this.debugMode) {
                    console.log('The promise resolved with the following data: ');
                    console.log(data);
                }

                spot = data;
                spot.offer_status = 3;

                this.offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);
                this.offerHandler.updateOffer(spot);

                this.reservationHandler = new ReservationHandler(this.alertCtrl, this.database, this.auth);
                this.reservationHandler.createReservation(spot);

                resolve();

            }).catch(() => {
                reject();
            })
        })
    }

    /**Checks a spot to see if the front of backend of the
     * request leaves enough time to carve up a new spot.
     * If there is time, it asks the owner if they wish
     * to create a new offer or decide later.  Later will
     * result in the user having to manually offer their spot.
     * 
     * @param offer The spot that should be checked for splicing.
     */
    private additionalOfferCheck(offer): Promise<any> {
        return new Promise((resolve) => {
            if (this.debugMode) {
                console.log('Checking to see if I can make more offers from your spot...');
            }

            if (((offer.request_end - offer.offer_start) > 3600000) || ((offer.offer_end - offer.request_end) > 3600000)) {

                if (this.debugMode) {
                    console.log('Your spot has more availability!');
                }
                resolve(this.createAdditionalOffer(offer));

//Leif - I took this out since they've already agreed to offer their spot... let's just slice and dice and carry on -Will

                // let alert = this.alertCtrl.create({
                //     title: 'Neighborly',
                //     subTitle: 'Generate Offer?',
                //     message: 'Accepting this spot will carve out '
                //     + ' Would you like me to offer you spot to fill (but not exceed) the additional time?',
                //     buttons: [
                //         // {
                //         //     text: 'Later',
                //         //     role: 'cancel',
                //         //     handler: () => {
                //         //         console.log('later clicked');
                //         //         resolve(offer);
                //         //     }
                //         // },
                //         {
                //             text: 'OK',
                //             role: 'cancel',
                //             handler: () => {
                //                 console.log('Approve clicked');
                //                 resolve(this.createAdditionalOffer(offer));
                //             }
                //         }
                //     ]
                // });

                // setTimeout(function () { alert.present() }, 500);

            } else {
                if (this.debugMode) {
                    console.log('Nope.  Your spot is the perfect size.');

                }
                resolve(offer);
            }
        });
    }

    /**Helper function that will create upto two additional spots given the
     * proper amount of time before and after the approval that has taken
     * place.  The OfferHandler is constructed and used from this class.
     * 
     * @param offer the spot you wish to split into additional spots.
     * @return Returns the spot as altered to update the start / end times after
     * being truncated.
     */
    private createAdditionalOffer(offer): Promise<any> {

        var tempOffers = new Array();

        for (var i = 0; i < 2; i++) {

            tempOffers.push(new Transaction(offer._1_offer_start, offer._2_offer_end, offer._3_request_start, offer._4_request_end,
                offer.building, offer.garage,
                offer.offer_end, offer.offer_start, offer.offer_status, offer.offer_weight,
                offer.owner, offer.request_end, offer.request_start, offer.request_user,
                offer.request_weight, offer.spot));
        }

        if ((offer.request_start - offer.offer_start) >= 3600000) {

            tempOffers[0].offer_end = offer.request_start; //- 300000;

            offer.offer_start = offer.request_start;

            if (this.debugMode) {
                console.log('We can add to the front end of the current request / offer: ');
                console.log('Offer Start: ' + offer.offer_start);
                console.log('Offer End: ' + offer.offer_end);
                console.log('TempOffer Start: ' + tempOffers[0].offer_start);
                console.log('TempOffer End: ' + tempOffers[0].offer_end);
            }

            this.offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);

            this.offerHandler.createOffer(tempOffers[0], 1);

        }

        if ((offer.offer_end - offer.request_end) >= 3600000) {

            tempOffers[1].offer_start = offer.request_end; //+ 300000;
            offer.offer_end = offer.request_end;

            if (this.debugMode) {
                console.log('We can add to the BACK end of the current request / offer: ');
                console.log('Offer Start: ' + offer.offer_start);
                console.log('Offer End: ' + offer.offer_end);
                console.log('TempOffer Start: ' + tempOffers[1].offer_start);
                console.log('TempOffer End: ' + tempOffers[1].offer_end);
            }

            this.offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);

            this.offerHandler.createOffer(tempOffers[1], 1);
        }

        if (this.debugMode) {
            console.log("Returning the offer to the calling function: ");
            console.log(offer);
        }
        return offer;
    }

}