
import { GenericDatabase } from "../providers/generic-database";
import { AlertController } from "ionic-angular";
import { AuthService } from '../providers/auth-service';
import { User_local as User } from '../providers/auth-service';
import { Reservation } from './library';
import { Transaction } from './library';
import { OfferHandler } from './offerhandler';
import * as moment from 'moment';

export class ReservationHandler {

    private debugMode: boolean = true;
    private userDetails: User;

    constructor(private alertCtrl: AlertController, private database: GenericDatabase,
        private auth: AuthService) {

        if (this.debugMode) {
            console.log('Building a new OfferHandler!');
        }

        this.userDetails = this.auth.getUserInfo();
    }

    /**This function UPDATES AN EXISTING RESERVATION
     * already present in the database.  Do not
     * use to CREATE NEW RESERVATIONS!!!!.
     * 
     * @param reservation The reservation you wish to create
     * as a record into the database.
     * @return returns a promies that indicates that
     * the proper database interaction took place.
     */

    updateReservation(reservation: Reservation): Promise<any> {

        return new Promise((resolve, reject) => {

            if (this.debugMode) {
                console.log("Updating the reservation, ", reservation);
            }

            this.database.update_DB_childByParent('/buildings/' + reservation.building + '/' + reservation.garage + '/' + "/spots/" + reservation.spot, "reservations", "reservation_" + reservation.reservation_start, reservation);

            resolve();

        });
    }


    /**THIS CREATES A NEW RESERVATION into the database.
     * DO NOT USE to update an existing RESERVATION; 
     * 
     * @param reservation the offer you wish to create
     * @return returns a promie that indicates the
     * proper database interaction took place.
     */
    createReservation(reservation): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.debugMode) {
                console.log("Reserving the spot : ", reservation, "beginning on ", reservation.offer_start);
            }

            let currentDate = new Date()

            var newReservation = new Reservation(moment(reservation.offer_start).local().toString(), moment(reservation.offer_end).local().toString(),
                reservation.building, reservation.garage, reservation.offer_start, reservation.offer_end,
                moment(currentDate.getTime()).local().toString(), " ", 3, reservation.request_user, reservation.spot);

            this.database.createEntry('buildings/' + reservation.building + '/' + reservation.garage + '/spots', reservation.spot + "/reservations/reservation_" + reservation.offer_start, newReservation);

            resolve();

        });
    }

    /**
     * Method that allows a renter to cancel their reservation.
     * Automatically slots the cancelled reservation
     * back into the offers table of the owner, merging as
     * necessary.
     * 
     * 
     * @param transaction The approved offer the renter wishes
     * to cancel as a Transaction type.
     * 
     * @return Returns a promise, .then() on succes, .reject(err) 
     * upon error 
     */
    renterCancel(transaction: Transaction): Promise<any> {

        return new Promise((resolve, reject) => {

            // setup query parameters
            // 
            // Build a generic "transaction" to hold the
            // merged data in
            //
            // frontEnd is the spot immediately before the
            // cancellation
            //
            // backEnd is the spot immediately after the
            // cancellation

            var table = 'buildings/' + transaction.building + '/' + transaction.garage + '/spots/' + transaction.spot + '/offers';

            var mergedOffer: Transaction;

            var frontEnd: Transaction = new Transaction(' ', ' ', ' ', ' ',
                transaction.building, transaction.garage,
                -1, -1, -1, -2, this.userDetails.email, -1,
                -1, ' ', -1, transaction.spot);
            var frontEndChild = 'offer_end';
            var frontEndValue: Number = transaction.offer_start;

            var backEnd: Transaction = new Transaction(' ', ' ', ' ', ' ',
                transaction.building, transaction.garage,
                -1, -1, -1, -2, this.userDetails.email, -1,
                -1, ' ', -1, transaction.spot);
            var backEndChild = 'offer_start';
            var backEndValue = transaction.offer_end;

            var deleteBack: boolean = false;
            var deleteReservation: boolean = false;

            /*
                First we check against the database to see if  
              
             */

            this.database.queryItemsEqualToQualifierAsPromise(table, frontEndChild, frontEndValue).then(result => {

                if (this.debugMode) {
                    console.log('The reservationHandler found this spot at the frontEnd of the renters cancellation request.'
                        + ' frontEnd result: ', result);
                    console.log('Now attempting to find the backEnd...');
                }

                // store the restul of the query in
                // a holding variable, frontEnd
                if (result[0] != undefined) {
                    frontEnd = result[0];
                }
                /* 
                    Next we want to query to find the backend.  We check to see if there exists
                    an offer_start === reservation_end
                */

                this.database.queryItemsEqualToQualifierAsPromise(table, backEndChild, backEndValue).then(result => {

                    if (this.debugMode) {
                        console.log('The reservatinHandler found this spot at the backEnd of the renters cancellation request.'
                            + ' backEnd result: ', result);
                    }

                    // store the result of the query in
                    // a holding variable, backEnd
                    if (result[0] != undefined) {
                        backEnd = result[0];
                    }
                    /*
                        As long as the frontEnd offer is still at status 1 (available)
                        then we want to start are merged offer with at this point.
                     */
                    if (frontEnd.offer_status == 1) {
                        if (this.debugMode) {
                            console.log('frontEnd was available, merging into single offer');
                        }

                        mergedOffer = new Transaction(frontEnd._1_offer_start, frontEnd._2_offer_end,
                            frontEnd._3_request_start, frontEnd._4_request_end,
                            frontEnd.building, frontEnd.garage,
                            frontEnd.offer_end, frontEnd.offer_start,
                            frontEnd.offer_status, frontEnd.offer_weight,
                            frontEnd.owner, frontEnd.request_end,
                            frontEnd.request_start, frontEnd.request_user,
                            frontEnd.request_weight, frontEnd.spot);

                        // check to see if the backEnd offer is available
                        // if it is, we set the mergedOffer end to this 
                        // backEnd.offer_end; ELSE we set it equal to the
                        // the reservation.offer_end
                        if (backEnd.offer_status == 1 && backEnd != undefined) {

                            if (this.debugMode) {
                                console.log('backEnd was available, merging into single offer');
                            }

                            mergedOffer._2_offer_end = backEnd._2_offer_end;
                            mergedOffer.offer_end = backEnd.offer_end;
                            mergedOffer.offer_weight = mergedOffer.offer_end - mergedOffer.offer_start;
                            deleteBack = true;
                            deleteReservation = true;

                        } else {

                            if (this.debugMode) {
                                console.log('backEnd was unavailable, merging the reservation into single offer');
                            }

                            mergedOffer._2_offer_end = transaction._2_offer_end;
                            mergedOffer.offer_end = transaction.offer_end;
                            mergedOffer.offer_weight = mergedOffer.offer_end - mergedOffer.offer_start;
                            deleteReservation = true;

                        }

                    }


                    /* 
                        If the frontEnd was not available, we move to see if the backEnd was available.
                        If it is available, we create mergedOffer using a combination of the reservation
                        and the backEnd offer.  backEnd offer will be only be used to get the appropriate 
                        _2_offer_end and offer_end.  Other fields are populated from the reservation 
                        (passed as transaction by the handler call signature).
                    */
                    else if (backEnd.offer_status == 1) {

                        if (this.debugMode) {
                            console.log('ReservationHandler did not find a frontEnd offer, but found a backEnd Offer.  ~ line 209')
                        }

                        mergedOffer = new Transaction(transaction._1_offer_start, backEnd._2_offer_end,
                            ' ', ' ', transaction.building, transaction.garage,
                            backEnd.offer_end, transaction.offer_start, 1, backEnd.offer_end - transaction.offer_start,
                            transaction.owner, -1, -1, ' ', -2, transaction.spot);

                        deleteBack = true;

                    } else {

                        if (this.debugMode) {
                            console.log('ReservationHandler did not find a frontEnd offer or a backEnd Offer. Updating'
                                + ' only the spot that needs to be cancelled  ~ line 220')
                        }

                        mergedOffer = new Transaction(transaction._1_offer_start, transaction._2_offer_end,
                            ' ', ' ', transaction.building, transaction.garage,
                            transaction.offer_end, transaction.offer_start, 1, transaction.offer_end - transaction.offer_start,
                            transaction.owner, -1, -1, ' ', -2, transaction.spot);

                    }

                    if (this.debugMode) {
                        console.log('The merged offer to insert into database, after cancellation (reservationHandler ~line 231) ', mergedOffer);
                    }


                    /*
                        Finally we need to update the reservation in the database.
                        Start by pulling the reservation down.
                    */
                    var table = 'buildings/' + transaction.building + '/' + transaction.garage + '/spots/' + transaction.spot + '/reservations';
                    var reservationChild = 'reservation_start'
                    var reservationChildValue: number = transaction.offer_start;
                    this.database.queryItemsEqualToQualifierAsPromise(table, reservationChild, reservationChildValue).then(result => {

                        if (this.debugMode) {
                            console.log('Searching for the reservation in reservationHandler ~ line 245 resulted in ', result);
                            console.log('deleteBack, ', deleteBack, '; deleteReservation, ', deleteReservation);
                        }

                        var reservationToUpdate: Reservation;

                        reservationToUpdate = new Reservation(result[0]._1_reservation_start, result[0]._2_reservation_end, result[0].building, result[0].garage,
                            result[0].reservation_start, result[0].reservation_end, result[0].reservation_confirmed, ' ', 5, result[0].renter, result[0].spot);

                        let currentDate = new Date()

                        reservationToUpdate.reservation_cancelled = moment(currentDate.getTime()).local().toString();

                        if (this.debugMode) {
                            console.log('The reservation to update! - ', reservationToUpdate);
                        }

                        this.updateReservation(reservationToUpdate).then(result => {
                            /*
                                we updated the offer to include the backEnd.
                                We need to delete the backEnd offer from the 
                                database. 
                            */

                            if (deleteBack) {

                                if (this.debugMode) {
                                    console.log('Deleting the backend from the reservationHandler due to cancellation');
                                    console.log('deleteBack, ', deleteBack);
                                }

                                var path = 'buildings/' + transaction.building + '/' + transaction.garage + '/spots/' + backEnd.spot + '/offers/offer_' + backEnd.offer_start;
                                this.database.deleteRecord(path);
                            }

                            /*
                                we updated the offer to include the both the back and frontEnd.
                                We need to delete the reservation offer from the 
                                database. 
                             */

                            if (deleteReservation) {

                                if (this.debugMode) {
                                    console.log('Deleting the reservation from the reservationHandler due to cancellation');
                                }

                                var path = 'buildings/' + transaction.building + '/' + transaction.garage + '/spots/' + transaction.spot + '/offers/offer_' + transaction.offer_start;
                                this.database.deleteRecord(path);
                            }

                            /*
                                Finally we need to insert the offer into the database.
                                We will call our offerHandler to update an offer.
                                We use UPDATE because we should always be manipulating
                                and offer that exists. 
                            */

                            var offerHandler = new OfferHandler(this.alertCtrl, this.database, this.auth);

                            offerHandler.updateOffer(mergedOffer);

                            resolve('Reservation successfully cancelled');

                        }).catch(err => {
                            reject('An error occurred attempting to cancel the reservation: ' + err.toString());
                        });

                    }).catch(err => {
                        reject('An error occurred attempting to cancel the reservation: ' + err.toString());
                    });

                }).catch(err => {
                    if (this.debugMode) {
                        console.log('The reservationHandler encountered the following error on backEnd query while'
                            + 'trying to cancel the renters request.  Error: ', err);
                    }
                    reject('An error occurred attempting to cancel the reservation: ' + err.toString());
                });

            }).catch(err => {
                if (this.debugMode) {
                    console.log('The reservationHandler encountered the following error on frontEnd query while'
                        + 'trying to cancel the renters request.  Error: ', err);
                }

                reject('An error occurred attempting to cancel the reservation: ' + err.toString());
            });


        });
    }

    /**
     * Method that allows an owner to cancel a reservation
     * of their spot.  This will cancel the reservation and
     * delete the offer form the database.  The owner needs
     * to make the space available again for the requested
     * time.
     * 
     * 
     * @param transaction The approved offer the owner wishes
     * to cancel as a Transaction type.
     * 
     * @return Returns a promise, .then() on succes, .reject(err) 
     * upon error 
     */
    ownerCancel(transaction: Transaction): Promise<any> {

        return new Promise((resolve, reject) => {

            // setup query parameters
            // 
            // Build a generic "transaction" to hold the
            // merged data in
            //
            // frontEnd is the spot immediately before the
            // cancellation
            //
            // backEnd is the spot immediately after the
            // cancellation


            var table = 'buildings/' + transaction.building + '/' + transaction.garage + '/spots/' + transaction.spot + '/reservations';
            var reservationChild = 'reservation_start'
            var reservationChildValue: number = transaction.offer_start;
            this.database.queryItemsEqualToQualifierAsPromise(table, reservationChild, reservationChildValue).then(result => {

                /*
                    build a new reservation based on the queries return.
                    We will update the specified parameters and then send
                    the object in its entirety back to the database to
                    ensure it is updated properly.
                */
                var reservationToUpdate: Reservation;

                reservationToUpdate = new Reservation(result[0]._1_reservation_start, result[0]._2_reservation_end, result[0].building, result[0].garage,
                    result[0].reservation_start, result[0].reservation_end, result[0].reservation_confirmed, ' ', 5, result[0].renter, result[0].spot);

                let currentDate = new Date()

                reservationToUpdate.reservation_cancelled = moment(currentDate.getTime()).local().toString();

                if (this.debugMode) {
                    console.log('The reservation to update! - ', reservationToUpdate);
                }

                this.updateReservation(reservationToUpdate).then(result => {

                    /*
                        We successfully updated the reservation.  It is now time
                        to delete the offer from the /spots/spot_###/offers/ table. 
                    */

                    if (this.debugMode) {
                        console.log('Deleting the offer associated with teh cancellation from the reservationHandler due to cancellation ~ line 400');
                    }

                    var path = 'buildings/' + transaction.building + '/' + transaction.garage + '/spots/' + transaction.spot + '/offers/offer_' + transaction.offer_start;
                    this.database.deleteRecord(path);




                    resolve('Reservation successfully cancelled');

                }).catch(err => {
                    reject('An error occurred attempting to cancel the reservation: ' + err.toString());
                });

            }).catch(err => {
                reject('An error occurred attempting to cancel the reservation: ' + err.toString());
            });
        });
    }

}

