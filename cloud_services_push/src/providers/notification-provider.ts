import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers } from '@angular/http';
import { WindowService } from './window-service';
import { AuthService } from './auth-service';
import { Platform, NavController, Nav } from 'ionic-angular'; //This is used to check if we're running ionic serve or if on a real device
import { ApproveHandler } from '../components/approvehandler';
import { ViewChild, Component } from "@angular/core";
import { AlertController } from "ionic-angular";
//import { Push as Push_native, PushObject, PushOptions } from "@ionic-native/push";
import { Badge } from '@ionic-native/badge';
import { Push, PushToken } from '@ionic/cloud-angular'; //Auth, User, UserDetails, IDetailedError,
import { GenericDatabase } from "../providers/generic-database";
import * as moment from 'moment';
import { App } from "ionic-angular";


/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NotificationProvider {
  //@ViewChild('myNav') navCtrl: NavController;  //this used to be Nav, not NavController
  //@ViewChild(Nav) nav2: Nav;
  navCtrl: any; //NavController

  private currentUser: String;
  //private displayedBadge: any;
  private debugMode: boolean = false;
  public enable_push: boolean = true;

  constructor(public http: Http,
    private windowService: WindowService,
    private platform: Platform,
    private alertCtrl: AlertController,
    private badge: Badge,
    private push_ionic: Push,
    private database: GenericDatabase,
    private auth: AuthService,
    private app: App
    //private navCtrl: NavController
    //private push_native: Push_native
  ) {

    console.log('Hello NotificationProvider Provider');
    this.currentUser = this.windowService.getKey('user', '');
    this.navCtrl = app.getActiveNav();

  }




  /**
   * Helper function for sending a notification / pushing a notification to an end user.
   * Must be supplied the end user and the notification message (e.g. You have 1 new message!).
   * 
   * @param toUser The user the notification needs to be sent to, as a string (CURRENTLY USING EMAIL ADDRESS!)
   * @param notificationMsg The notification the end user should see.
   */

  sendNotification(toUser: String, notificationMsg: String, notifType: String, data: any) {


    ///////////////////////////////////////////////////////////////////////

    this.database.queryDB_single_item_as_Promise("users/", toUser.replace(/\./g, ",")).then(result => {

      if (this.debugMode) {
        console.log("All currently made offers for the spot ", result);
        console.log("result");
        console.log("this is info about", toUser);
        console.log(result);
      }
      let number: number;
      for (var i = 0; i < result.length; i++) {
        if (result[i].$key === 'badge') {
          number = +result[i].$value;
          if (this.debugMode) {
            console.log("found it");
            console.log(result[i]);
            console.log("the number is", number);
          }
        }
      }


      console.log("the badge number is:", number);
      // This section sends the notification via ionic cloud for each message
      var link = "https://api.ionic.io/push/notifications";
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyODc0ZWI2Yy0xNWIyLTRhMDgtODg5Ni0yNzAyY2I5ODQxNzYifQ.d6pZNB2-5GyDOl9ke3HUmPreA6DH06EZp7ni8_g6fNs');

      var payload = {
        //"tokens": "foj0GHATkec:APA91bFYduDuE9DLAkI9pQIrmaieMKutKIopVzWBMDx9lrDe7n7N2xbohv_cK_MtgIoF8TNMU4CeSlhC9CvqvS7gZSOH8b9M27ps49O3UAmH6-60Q7AWQq_wGrWTvIgA1erWfCVSlkva",
        "title": "Neighborly",
        "emails": toUser,
        "profile": "push_notification",
        "content-available": 1,
        "notification": {
          "content-available": 1,
          "message": notificationMsg,
          //"sound": "default",
          "android": {
            //"soundname": "default",
            "content-available": 1,
            "title": notifType,
            "payload": { data }
          },
          "ios": {
            "content-available": 1,
            "sound": "default",
            "title": notifType,
            "badge": number + 1,  // I set this to one for the case where we need to use the ionic cloud notification system.  
            // This way, a message will show "1" and then it needs to be cleared in another screen
            "payload": { data }
          }
        }
      }

      var jsonString = JSON.stringify(payload);
      this.http.post(link, jsonString, { headers: headers })
        .subscribe(res => {
          console.log(res.json());
        }, (err) => {
          console.log(err);
        });
      console.log(jsonString);

      this.database.update_DB_childByParent("/users/", toUser.replace(/\./g, ","), "badge", number + 1);
    }).catch(() => {
      console.log("nope nope nope");
    });

    // end ionic push notifications
  } //end sendNotification()

  unregister() {
    this.push_ionic.unregister();
  }

  register() {
    // much of this code is from here:
    // https://medium.com/@ankushaggarwal/push-notifications-in-ionic-2-658461108c59
    // first we need to see if this is an ionic platform
    var ionicServe = false;
    if (!this.platform.is("cordova")) {
      if (this.debugMode) {
        console.log("running ionic serve");
      }
      ionicServe = true;
    }
    else {
      if (this.debugMode) {
        console.log("not running ionic serve");
      }
    }
    if (!ionicServe && this.enable_push) {

      this.push_ionic.register()
        .then((t: PushToken) => {
          if (this.debugMode) {
            console.log("pushtoken: ", t);
            console.log("pushtoken id: ", t.id);
            console.log("pushtoken registered: ", t.registered);
            console.log("pushtoken saved: ", t.saved);
            console.log("pushtoken token: ", t.token);
            console.log("pushtoken type: ", t.type);
          }
          return this.push_ionic.saveToken(t);
        }).then((t: PushToken) => {
          if (this.debugMode) {
            console.log('Token saved:', t.token);
          }
        });

      this.push_ionic.rx.notification()
        .subscribe((msg) => {
          console.log("received a text message");
          console.log(msg);
          var pagename = this.navCtrl.getActive().name; // This grabs the name of the current page. It will be used to prevent messages from arriving while a user has the chat page open.
          console.log(pagename);

          let payload: any;
          payload = msg.payload;
          if (msg.title == "request") {
            let alert = this.alertCtrl.create({
              title: 'Neighborly',
              //subTitle: msg.title,
              message: payload.data.htmlMessage,
              cssClass: 'custom-alert-danger',
              buttons: [
                {
                  text: 'Later',
                  role: 'cancel',
                  handler: () => {
                    if (this.debugMode) {
                      console.log('later clicked');
                    }

                  }
                },
                {
                  text: 'Deny',
                  handler: () => {
                    if (this.debugMode) {
                      console.log('Deny clicked');
                    }
                    alert.dismiss();
                    this.showConfirmReject(payload.data.spot);
                  }
                },
                {
                  text: 'Approve',
                  handler: () => {
                    if (this.debugMode) {
                      console.log('Approve clicked');
                      console.log("notification-provider.register-> Approve button handler");
                      console.log("payload.data");
                      console.log(payload.data);
                    }
                    alert.dismiss();
                    this.showConfirmApprove(payload.data.spot);
                  }
                }
              ]
            });
            alert.present();
          }
          if (msg.title == "notify") {
            let alert = this.alertCtrl.create({
              title: 'Neighborly',
              //subTitle: msg.title,
              message: payload.data.htmlMessage,
              cssClass: 'custom-alert-happy',
              buttons: ['Dismiss']
            });
            alert.present();
          }
          if (msg.title == "chat") {
            if (pagename != "ConversationPage") {
              let alert = this.alertCtrl.create({
                title: 'Neighborly',
                message: payload.data.htmlMessage,
                cssClass: 'custom-alert-happy',
                buttons: ['Dismiss']
              });
              alert.present();
            }
          }

          if (msg.title == "alert") {
            let alert = this.alertCtrl.create({
              title: 'Neighborly',
              //subTitle: msg.title,
              message: payload.data.htmlMessage,
              cssClass: 'custom-alert-danger',
              buttons: ['Dismiss']
            });
            alert.present();
          }
        });
      // let configuration = { android: { senderID: "xxxxxxxx" } }
      // this.push_ionic.plugin.init(configuration).on("notification"){
      //   alert("worked");
    }
  }

  showConfirmReject(spot) {
    let deny = this.alertCtrl.create({
      title: 'Wait a second...',
      message: 'Are you sure that you want to reject this user from using your parking spot?',
      cssClass: 'custom-alert-danger',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            //Call library function to perform all deny functions
            if (this.debugMode) {
              console.log(spot);
              console.log("spot printed from pendingReserved");
            }
            deny.dismiss()
            this.deny(spot);
          }
        }
      ]
    });
    setTimeout(function () { deny.present() }, 500);
  }

  deny(spot): void {
    //change the offer_status back to 1 (from 2)
    if (this.debugMode) {
      console.log(spot);
      console.log("***spot.spot***************", spot.spot);
      console.log("spot.offer_start", spot.offer_start);
    }

    this.database.update_DB_childByParent("/buildings/" + spot.building + "/" + spot.garage + "/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "offer_status", 1);

    // load up the notification
    let start = spot._3_request_start;
    let end = spot._4_request_end;



    var notificationMessage = "Your parking request was DENIED from \n"
      + moment(start).format('ddd, MMM Do YYYY [at] h:mm a')
      + "\nTo: \n"
      + moment(end).format('ddd, MMM Do YYYY [at] h:mm a');

    var htmlMessage = `<p>Your parking request was DENIED from</p>` +
      `<p>` + moment(start).format('ddd, MMM Do YYYY [at] h:mm a') + `</p>` +
      `<p> To: </p>` +
      `<p>` + moment(end).format('ddd, MMM Do YYYY [at] h:mm a') + `</p>`;

    var payload = {
      "spot": spot,
      "htmlMessage": htmlMessage
    }

    let data = "";
    this.sendNotification(spot.request_user, notificationMessage, "notify", payload);
  } //end deny()

  /**Builds a new component ApproveHandler that will
   * handle the duties of approving a spot for a user.
   * ApproveHandler includes another object OfferHandler
   * which is used to make new offers if it is needed.
   * 
   * @param spot The spot you wish to approve.
   */

  approve(spot): void {
    if (this.debugMode) {
      console.log("Notification-provider: approving the spot: ");
      console.log(spot);
    }

    let approveHandler = new ApproveHandler(this.alertCtrl, this.database, this.auth);

    approveHandler.approve(spot).then(() => {

      let start = spot._3_request_start;
      let end = spot._4_request_end;

      var notificationMessage = 'Your parking request was APPROVED =) from \n' +
        moment(start).format('ddd, MMM Do YYYY [at] h:mm a') +
        '\n To: \n' +
        moment(end).format('ddd, MMM Do YYYY [at] h:mm a')


      var htmlMessage = `<p>(= Your parking request was APPROVED =) from </p>`
        + `<p>` + moment(start).format('ddd, MMM Do YYYY [at] h:mm a') + `</p>`
        + `<p>To: </p>`
        + `<p>` + moment(end).format('ddd, MMM Do YYYY [at] h:mm a') + `</p>`
        + `<p><img src="assets/img/sun.png" style="width: 25%; height: 25%"/></P>`

      var payload = {
        "spot": spot,
        "htmlMessage": htmlMessage
      }

      this.sendNotification(spot.request_user, notificationMessage, "notify", payload);

    }).catch(() => {
      let failure = this.alertCtrl.create({
        title: 'Something went wrong...',
        message: 'We were unable to confirm the reservation.  Please approve again.',
        cssClass: 'custom-alert-danger',
        buttons: [
          {
            text: 'Dismiss',
            handler: () => {
              console.log('Confirmed');
              failure.dismiss();
            }
          }
        ]
      });
      setTimeout(function () { failure.present() }, 500);
    });
  }


  showConfirmApprove(spot) {
    let approve = this.alertCtrl.create({
      title: 'Double checking...',
      message: 'Are you sure that you wish to lend out your parking spot?',
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            //Call library function to perform all approve functions
            if (this.debugMode) {
              console.log("spot printed from pendingReserved");
              console.log(spot);
            }
            approve.dismiss();
            this.approve(spot);
          }
        }
      ]
    });

    setTimeout(function () { approve.present() }, 500);

  }

}




