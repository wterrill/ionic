import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
//import { Database } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import { WindowService } from './window-service';
import { AngularFire, FirebaseListObservable } from 'angularfire2';  //FirebaseObjectObservable



class conversation implements Object {
  chatId: string
  user: string
}


@Injectable()
export class ConversationsDatabase {


  // ************           DANGER LEIF BROCKMAN DANGER **********************
  //
  // https://www.joshmorony.com/building-mobile-apps-with-ionic-2/observables-in-ionic2.html

  public existingChatId: string;
  convList: FirebaseListObservable<any[]>;
  public currentUser: string;
  public conversations: Array<conversation>;
  public conversationStream$: Observable<any>;
  private debugMode: boolean = false;

  constructor(public http: Http, public af: AngularFire, private windowService: WindowService) {
    this.existingChatId = null;
  }


  //Get conversations does these things:

  getConversations() {
    //this.conversationsConnect();
    this.currentUser = this.windowService.getKey('user', ''); //gets the logged in user name
    if (this.currentUser != null) {
      if (this.debugMode) {
        console.log("current user", this.currentUser);
      }

      //This creates an observable used to query the firebase database for results where user1 == currentUser
      //https://github.com/angular/angularfire2/blob/master/docs/4-querying-lists.md
      const queryObservable1$ = this.af.database.list('/conversations/', {
        query: {
          orderByChild: "user1",
          equalTo: this.currentUser
        }
      });

      //This creates an observable used to query the firebase database for results where user2 == currentUser
      const queryObservable2$ = this.af.database.list('/conversations/', {
        query: {
          orderByChild: "user2",
          equalTo: this.currentUser
        }
      });

      //.combineLatest makes a single observable from the constituent observables.
      const combined$ = Observable.combineLatest(queryObservable1$, queryObservable2$);


      //Here's where the magic happens.  The .map manipulates the results of the combined$ observable which,
      //in itself creates a new observable.  This final observable is what will be passed to the Conversations.ts page, 
      //so it needs to deliver a list of usernames and chatids.
      this.conversationStream$ = combined$.map(response => {
        //This is the beginning of the combined$ observable result
        var conversations: {
          user: string,
          chatId: string
        }[] = new Array();
        var flatresponse = [].concat.apply([], response); //this flattens the array (it was an array of arrays originally) 
        //http://stackoverflow.com/questions/10865025/merge-flatten-an-array-of-arrays-in-javascript
        for (var i = 0; i < flatresponse.length; i++) { //this loops through flatresponse and populates the conversations list which is what is returned.
          if (flatresponse[i].user1 != this.currentUser) {
            conversations.push({ user: flatresponse[i].user1, chatId: flatresponse[i].$key })
          } else {
            conversations.push({ user: flatresponse[i].user2, chatId: flatresponse[i].$key })
          }
        }
        // this is saved so that we can parse it in "conversationExists" below without going to the database again.
        this.conversations = conversations;
        if (this.debugMode) {
          console.log("conversations in firebase-database.ts", conversations);
        }
        return conversations //the conversations list is returned as an observable.
      })
    } //end of if (this.currentUser != null)
  }//end of getConversations()


  /**
   * Returns a boolean as a promise if their exists
   * a combination of user and this.currentUser in
   * the conversation database.  If true, there is no
   * need to create a new record in the conversation 
   * collection.  If false, then a new record should
   * be created in the collection.
   * 
   * @param user This should be the value of the user
   * that the the current user is trying to contact.
   */

  conversationExists(user: String): boolean {
    //this.conversationsConnect();

    if (this.currentUser != null) {
      if (this.debugMode) {
        console.log("from converssationExists - Conversations:", this.conversations)
      }
      var findresult = this.conversations.find(myObj => myObj.user === user);
      if (this.debugMode) {
        console.log("findresult value =", findresult);
      }
      if (user === this.currentUser) {
        alert("You don't need me to have a conversations with yourself!");
      }

      else if (findresult == null) {
        if (this.debugMode) {
          console.log("Conversation exists returned FALSE");
        }
        return false;
      }
      else if (findresult.user === user) {
        if (this.debugMode) {
          console.log("Conversation exists returned TRUE");
        }
        this.existingChatId = findresult.chatId;
        return true;
      }
      else {
        if (this.debugMode) {
          console.log("didn't catch it. Returned false");
        }
        return false;
      }
    }
  }//ending for conversationExists

  /**
   * Helper function to create a new converstation recrod 
   * in the conversation collection.  Will setup a new conversation
   * between two users.
   * 
   * @param firstUser First user associated with the chat.  Typically the user
   * initiating the conversation.
   * @param secondUser The second user associated with the chat.  Typically the
   * recipient at the beginning of the conversation.
   * @param uChatId A unique id of the conversation.  user1_user2 is the Typically
   * chatId naming representation.  This will be the id stored in the id field
   * of the conversation collection.
   */


  createConversation(firstUser: string, secondUser: string, uChatId: string) {
    if (this.debugMode) {
      console.log("Attempting to insert user1: ", firstUser, " user2: ", secondUser, " chatId: ", uChatId);
    }
    const items = this.af.database.list('/conversations/');
    items.update(uChatId, { user1: firstUser, user2: secondUser });
    const messages = this.af.database.list('/messages/');
    messages.update(uChatId, {});
  }//ending for createConversation


  /**
   * Resets the conversationDB to its constructor state
   * reinitializing variables.
   */
  reset() {
    this.existingChatId = null;
  }
}//the ending for the class
