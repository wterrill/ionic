import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
//import { Database } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import { WindowService } from './window-service';
import { AngularFire, AngularFireDatabase, FirebaseListObservable } from 'angularfire2'; //FirebaseObjectObservable

declare var firebase: any;

@Injectable()
export class GenericDatabase {
  public existingChatId: string;
  public currentUser: string;
  //public queryDB_single_item$: FirebaseListObservable<any[]>;
  public genericQueryStream$: any;
  //private tableQueryResultSet: any;
  private debugMode: boolean = false;

  constructor(public http: Http, public af: AngularFire, private windowService: WindowService, private database: AngularFireDatabase) {

    console.log("Constructing a new generic database");
    this.currentUser = this.windowService.getKey('user', ''); //gets the logged in user name

  }



  createEntry(table, record, data) {
    // This creates entries in the firebase database. 
    // - table is the string name of the top-level branch of the database (users, messages, etc.) 
    // - record is the string name of the individual record within the table (i.e. username, message number)
    // - data is a structure that holds all of the lower level information, in the form of 
    //   { datafield1: "string1", datafield2: "string2", datafield3: {subdatafield1: "string3", subdatafield2:"string4"} }
    //
    // example: 
    //     data = { user1: "wterrill", user2: "it works", realnames: {user1: "Will", user2:"Leif"}}
    //     this.gdb.createEntry("table","record", data);
    // 
    // would create the following database entry:
    // Neighborly-577cs    <-- Database itself.  Configured elsewhere.
    // |_ table
    //      |_ record
    //           |_ user1: "wterrill"
    //           |_ user2: "lbrockman"
    //           |_ realnames
    //                |_ user1: "Will"
    //                |_ user2: "Leif"
    //
    if (this.debugMode) {
      console.log("inserting into table ", table, " with record: ", record, "with data", data);
    }
    const items2 = this.af.database.list(table);
    items2.update(record, data);
  }

  // I realize now what a horrible method this actually was.  RIP.

  // queryDB_single_item(table: String, item: String) {
  //   //This creates and observable using the table and item information passed to it, and stores
  //   // the observable in queryDB_single_item$ to allow the pages to subscribe to it.
  //   if (this.debugMode) {
  //     console.log("queryDB_single entry for table ", table, " and item ", item);
  //   }
  //   this.queryDB_single_item$ = this.af.database.list("/" + table + "/" + item,
  //     {
  //       query:
  //       {
  //         //you don't need anything here for single queries
  //       }
  //     });
  // }
  // promise basic use: this.database.queryDB_single_item_as_Promise("invitations/",this.invitationCredentials.invitationCode).then((result)=>{}).catch((error)=>{});

  queryDB_single_item_as_Promise(table: String, item: String): Promise<any> {
    //This creates and observable and wraps it into a promise ensuring
    // that the calling user gets the final data from the database
    // using the table and item information passed to it

    //NOTE: despite the fact that it is name "single item" it does NOT return just a single
    // item.  It returns an array.  In the code that calls it, you need to cycle through to 
    // find the specific item you're looking for.
    if (this.debugMode) {
      console.log("queryDB_single entry for table ", table, " and item ", item);
    }

    return new Promise((resolve, reject) => {

      this.af.database.list("/" + table + "/" + item,
        {
          query:
          {
            //you don't need anything here for single queries
          }
        }).subscribe(data => {
          console.log('Data from the query', data);
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

  /**
   * Queries the database for a specific subset from the table provided to
   * return those who match a specific child by the child value.  
   * 
   * ex.
   * table = spots/spot_217/offers
   * child = offer_weight
   * value = 36000
   * 
   * Expected value is all offers who have a weight of 36000.
   * 
   * @param table The table you wish to query against
   * @param child the child index you wish to search upon (Field name)
   * @param value value of the child to match
   * 
   */
  queryItemsEqualToQualifierAsPromise(table: String, child: string, value: any): Promise<any> {
    //This creates and observable and wraps it into a promise ensuring
    // that the calling user gets the final data from the database
    // using the table and item information passed to it

    if (this.debugMode) {
      console.log("queryItemsByQualifierAsPromise, parameters are table: ", table,
        ", child: ", child, ", and value : ", value);
    }

    return new Promise((resolve, reject) => {

      this.af.database.list("/" + table,
        {
          query:
          {
            orderByChild: child,
            equalTo: value
          }
        }).subscribe(data => {
          console.log('Items equal to ' + child + ' with a value of ' + value + ' returned: ', data);
          resolve(data);
        }, err => {

          if (this.debugMode){
            console.log ('Error message in database query: ', err.message);
          }

          if (err.message == 'backEnd is undefined') {
            resolve(undefined);
          } else if (err.message == 'frontEnd is undefined') {
            resolve(undefined);
          } else {
            reject(err);
          }
        })
    })
  }

  queryDB_single_table(table: String) {
    if (this.debugMode) {
      console.log("Querying for the table ", + table + ": ");
      console.log(table);
    }
    this.af.database.list("/" + table, {
      query: {

      }
    }).subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.genericQueryStream$ = Observable.create(observer => {
        try {
          observer.next(queryResultSet);
          observer.onCompleted();
        } catch (error) {
          observer.onError(error);
        }
      })
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
  }

  deleteRecord(path: any) {
    if (this.debugMode) {
      console.log("reached deleteRecord");
      console.log(path);
    }
    const items3 = this.af.database.object(path);
    items3.remove();
  }



  queryDB_table_byChildEqual(table: String, child: any, childValue: any) {
    /**
     * 
     * @param table the table to start the search within
     * @param child the parameter you want to limit results by (e.g. ID, name, address, status, etc.)
     * @param childValue the value you want that parameter to be EQUAL too (e.g. SELECT FROM table WHERE child == childValue)
     */
    if (this.debugMode) {
      console.log("Querying for the table " + table + ": ");
      console.log(table);
      console.log("and the children " + child + " whose values equal: " + childValue);
    }
    return this.af.database.list("/" + table, {
      query: {
        orderByChild: child,
        equalTo: childValue
      }
    })//.subscribe(queryResultSet => {
    //   if (this.debugMode) {
    //     console.log("The query returned: ");
    //     console.log(queryResultSet);
    //   }
    //   this.genericQueryStream$ = Observable.create(observer => {   //I'm still not sure about this...
    //     try {
    //       observer.next(queryResultSet);
    //       observer.onCompleted();
    //     } catch (error) {
    //       Observable.throw(error);
    //     }
    //   })
    // }, err => {
    //   console.log("Error with query.  Error code is: ");
    //   console.log(err);
    // });
  }

  update_DB_childByParent(path: String, key: any, childId: any, newChildValue: any): boolean {
    /**
     * Returns true if the update was succesful and false otherwise.
     * 
     * @param path path within the database that data is located (/users or /spots/spot253)
     * @param key The key the child exists on.  If path is /spots/spot253/offers key would be the offer_epoch you wish to update a child on.
     * @param childId The child you want to update.  spots/spot253/offers, key of offer_153, child offer_status would update the offer status property on offer_153
     * @param newChildValue Value you wish to set the child too.
     * Some examples:
     * 
     * this.database.update_DB_childByParent("/","debug",moment().unix().toString(),debug);  This puts the value at the highest level under "debug"
     * this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
     *
     */

    var success: boolean;
    var db = firebase.database().ref(path).child(key);

    if (this.debugMode) {
      console.log("The database is: ");
      console.log(db);
    }

    if (db != undefined || db != null) {

      if (this.debugMode) {
        console.log("Updating the status!");
      }
      db.update(
        { [childId]: newChildValue }
      )
      success = true;

    } else {

      console.log("Error finding data in database.");
      success = false;

    }

    if (success) {
      return true;
    } else {
      return false;
    }

  }



}
