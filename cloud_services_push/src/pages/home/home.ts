import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { MessagingPage } from '../messaging/messaging';
import { LoginPage } from '../login/login';
import { ConversationsPage } from '../conversations/conversations';
import { MapPage } from '../map/map';
import { ImagemapPage } from '../imagemap/imagemap';
import { SvgmapPage } from '../svgmap/svgmap';
import { MapzoomPage } from '../mapzoom/mapzoom';
import { SvgzoomPage } from '../svgzoom/svgzoom';
import { QueryPage } from '../query/query';
import { LandingPage } from '../landing/landing';


// import {
//   Push,
//   PushToken
// } from '@ionic/cloud-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //toggleNew: boolean = false;
  //tokenvalue: string;
  //token: PushToken;

  constructor(public navCtrl: NavController) {
    console.log("entering home page");
  }
  
  gotosignup(){
    this.navCtrl.push(SignupPage);
  }

  gotologin(){
    this.navCtrl.push(LoginPage);
  }


  gotosendmessage(){
     this.navCtrl.push( MessagingPage );   
   }

   gotoconversations() {
     this.navCtrl.push(ConversationsPage);
   }

  gotomap() {
     this.navCtrl.push(MapPage);
   }
  gotoimagemap() {
     this.navCtrl.push(ImagemapPage);
   }
  gotosvgmap() {
    this.navCtrl.push(SvgmapPage);
  }

  gotomapzoom(){
    this.navCtrl.push(MapzoomPage);
  }

  gotosvgzoom(){
    this.navCtrl.push(SvgzoomPage);
  }

  gotoquery(){
    this.navCtrl.push(QueryPage);
  }
  gotolanding(){
    this.navCtrl.push(LandingPage)
  }

}

