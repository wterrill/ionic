    import { Component, ViewChild } from '@angular/core';
    import { NavController, NavParams } from 'ionic-angular';
    import { GenericDatabase } from '../../providers/generic-database';
    import { ConversationsDatabase } from '../../providers/conversations-database';
    import { WindowService } from '../../providers/window-service';
    import { MessageModalPage } from '../message-modal/message-modal';
    import * as moment from 'moment'


    @Component({
      selector: 'page-userinfo',
      templateUrl: 'userinfo.html'
    })

    export class UserInfoPage {
      private user
      private display
      private display2
      private debugMode: boolean = true;
      private currentUser: String;

      constructor(
        private navCtrl: NavController,
        private navparams: NavParams,
        private database: GenericDatabase,
        private conversationDb: ConversationsDatabase,
        private windowService: WindowService
      ) {
      this.user = navparams.data;
      this.database.queryDB_single_item_as_Promise("users/",this.user.replace(/\./g, ","))
      .then((x)=>{
        this.display = x; 
        
        var result = x.reduce(function(map, obj) {
        map[obj.$key] = obj.$value;
          return map;
        }, {});
        console.log("*****result*****")
        console.log(result)


        var car_body = (result.body == "4sedan" ? "4-Door Sedan" : result.body); 
        car_body = (car_body == "2sedan" ? "2-Door Coupe" : car_body);
        car_body = (car_body == "Wagon" ? "Station Wagon" : car_body);
        car_body = (car_body == "SportsCar" ? "Sports Car" : car_body);

        var message = `<p>First Name: ` + result["firstName"].charAt(0).toUpperCase() + result["firstName"].slice(1) + `</p>` +
        `<p>Last Name: ` + result["lastName"].charAt(0).toUpperCase() + result["lastName"].slice(1) + `</p>` +
        `<p>Unit Number: ` + result.unit + `</p>` +
        `<p>Phone Number: ` + result.telephone  + `</p>` +
        `<p>Car Make/Model: ` + result.make_model  + `</p>` +
        `<p>Body Style: ` + car_body + `</p>` +
        `<p>Car Color: ` + result.color + `</p>` +
        `<p>License Plate State: ` + result.plateState + `</p>` +
        `<p>License Plate Number: ` + result.plate + `</p>` +
        `<p>Insurance Company: ` + result.insurance + `</p>` 

        console.log(message);
        this.display2 = message;

      })
      this.currentUser = windowService.getKey('user', '');
      }

   startConversation(user) {

    // variable datatype that we can pass to
    // the messaging page which stores are pertinent
    // message creation information
    var selectedUser: {
      user: any;
      chatId: string;
    } = { user: "null", chatId: "null" };

    var exists = this.conversationDb.conversationExists(user);
    if (this.debugMode) {
      console.log("listItem username is *-*-", user)
    }



    if (exists) {
      if (this.debugMode) {
        console.log("The conversation exists, pass chatId");
      }
      selectedUser.chatId = this.conversationDb.existingChatId;
      selectedUser.user = user;
    } else {
      // som basic string manipulation to remove @ and . from name
      // the ionic db services will not allow these characters in 
      // the name of a collection
      var user1First = this.currentUser.replace("@", "");
      var user1Second = user1First.replace(/\./g,"");

      var user2First = user.replace("@", "");
      var user2Second = user2First.replace(/\./g,"");

      selectedUser.chatId = user1Second.toString().toLowerCase() + "_" + user2Second.toString().toLowerCase();

      if (this.debugMode) {
        console.log("Attempt to insert the record into conversation collection.", );
      }
      this.conversationDb.createConversation(this.currentUser.toString(), user.toString().toLowerCase(), selectedUser.chatId);
    }
    if (this.debugMode) {
      console.log("Passing the selected user to the modal page: ", selectedUser);
    }
    selectedUser.user = user;
    this.conversationDb.reset();
    this.navCtrl.push(MessageModalPage, selectedUser);
  }


    }



