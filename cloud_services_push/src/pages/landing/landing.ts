import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Auth } from '@ionic/cloud-angular'; //User, UserDetails, IDetailedError, Push, PushToken 
import { WindowService } from '../../providers/window-service';
import { DatastreamService} from '../../providers/datastream-service';
import { ManagementPage } from '../management/management';
import { OfferPage } from '../offer/offer';
import { RequestPage } from '../request/request';
import { SettingsPage } from '../settings/settings';
import { LoginPage } from '../login/login';
import { OfferPendingReservedPage } from '../offerPendingReserved/offerPendingReserved';
import { RequestPendingReservedPage } from '../requestPendingReserved/requestPendingReserved';
import { AppVersion } from '@ionic-native/app-version';
import { AlertController } from "ionic-angular";
import { NotificationProvider } from '../../providers/notification-provider';
import { Badge } from '@ionic-native/badge';
import { ConversationsPage } from '../conversations/conversations';
/*
  Generated class for the Landing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class LandingPage {
  appName: String;
  packageName: String;
  versionNumber: String;
  versionCode: String;
  badges: number;
  nospot: boolean = true;
  spot: String;


  username: String;
  private debugMode: boolean = false;
  constructor( 
    public navCtrl: NavController,
    public navParams: NavParams,
    private windowService: WindowService,
    private auth: Auth,
    private app: AppVersion,
    private alertCtrl: AlertController, 
    private notificationprovider: NotificationProvider,
    private badge: Badge,
    private platform: Platform, 
    private datastream: DatastreamService) 
    
    {
    this.datastream.initializeStreams();
    this.username = windowService.window.localStorage.getItem('user');
    this.spot = windowService.window.localStorage.getItem('spot');
    this.spot = this.windowService.getKey('spot', '');
    this.auth = navParams.data;
    if (this.debugMode) {
      console.log("entering landing page");

    }

    this.getdata();
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');

  }

  ionViewWillEnter() {
         //this runs every time that the page is entereed.  
             if (this.platform.is("cordova")) {
      this.badge.get().then((x) => {
        this.badges = x;
      });
    }
    console.log("IonViewWillEnter");

  }
 


   logOut() {
            //this.notificationprovider.unregister();  //moved to settings
            this.auth.logout();
            this.navCtrl.push(LoginPage); 
   }

  goToOffer() {
    this.navCtrl.push(OfferPage);
  }

  goToRequest() {
    this.navCtrl.push(RequestPage);
  }

  goToConversations() {
    this.navCtrl.push(ConversationsPage);
  }

  goToManagement() {
    this.navCtrl.push(ManagementPage);
  }

  goToSettings() {
    this.navCtrl.push(SettingsPage);
  }

  goToOfferPending() {
    this.navCtrl.push(OfferPendingReservedPage);
  }

  goToRequestPending() {
    this.navCtrl.push(RequestPendingReservedPage);
  }

  getdata() {
    console.log("entering getdata");
    // this.app.getAppName().then((appName) => {
    //   this.appName = appName;
    //   console.log(appName)
    // }).catch((err) => {
    //   console.log(err);
    // })

    // this.app.getPackageName().then((packageName) => {
    //   this.packageName = packageName
    //   console.log(packageName)
    // }).catch((err) => {
    //   console.log(err);
    // })

    this.app.getVersionNumber().then((versionNumber) => {
      this.versionNumber = versionNumber
      console.log(versionNumber)
    }).catch((err) => {
      console.log(err);
    })

    // this.app.getVersionCode().then((versionCode) => {
    //   this.versionCode = versionCode
    //   console.log(versionCode);
    //   console.log("**END**");
    // }).catch((err) => {
    //   console.log(err);
    // })
  }
  about() {
    let alert = this.alertCtrl.create({
      title: "About Neighborly:",
      subTitle: '',
      cssClass: 'custom-alert-danger',
      message: `<p>Neighborly was created for Metropolitan Place by William Terrill and Leif Brockman.</p>
      <p>We hope you find it useful! </p>
      <p>You are running test version: ` + this.versionNumber + `</p>
      <img src="https://ih1.redbubble.net/image.314292782.4334/flat,550x550,075,f.u1.jpg" style="width: 50%; height: 50%"/>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


}