import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NavController, AlertController, LoadingController, Loading, NavParams } from 'ionic-angular'; //Loading
import { AuthService } from '../../providers/auth-service';
import { SpotValidator } from '../../validators/spotValidator';
import { SpotExistsValidator } from '../../validators/spotExistsValidator';
import { GarageValidator } from  '../../validators/garageValidator';
import { EqualValidator } from '../../validators/equalValidator';
import { AngularFire } from 'angularfire2';
import { LoginPage } from '../login/login';
import { DebugMessage } from '../../components/library';
import { GenericDatabase } from '../../providers/generic-database';
import * as moment from 'moment'

// https://www.joshmorony.com/username-availability-with-an-asynchronous-validator-in-angular/

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})

export class SignupPage {
  @ViewChild('signupSlider') signupSlider: any;
  signUpForm: FormGroup;
  submitAttempt: boolean = false;
  createSuccess = false;
  private loading: Loading;
  private badconnection: boolean = true;

  private debugMode: boolean = true;
  registerCredentials = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    building: '',
    garage: '',
    spot: '',
    unit: '',
    telephone: '',
    insurance: '',
    policy: '',
    make_model: '',
    body: '',
    color: '',
    plateState: '',
    plate: '',
    badge: ''
  };


  constructor(
    private navCtrl: NavController,
    private authprovider: AuthService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public database: GenericDatabase,
    public formBuilder: FormBuilder,
    private af: AngularFire,
    public spotExistsValidator: SpotExistsValidator,
    public navParams: NavParams
  ) {

    this.signUpForm = formBuilder.group({
      email: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$'), Validators.required])],
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required, EqualValidator('password')]],
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      building: ['', Validators.required],
      garage: ['', GarageValidator.isValid],
      spot: ['', [SpotValidator.isValid], spotExistsValidator.checkSpotExists.bind(spotExistsValidator)],
      unit: ['', Validators.required],
      telephone: ['', Validators.compose( [ Validators.minLength(10), Validators.maxLength(10)] ) ],
      insurance: [''],
      policy: [''],
      make_model: ['', Validators.required],
      body: ['', Validators.required],
      color: ['', Validators.required],
      plateState: ['', Validators.required],
      plate: ['', Validators.required]
    });

    //set default values for selects:
    this.registerCredentials.plateState = "IL"
    this.registerCredentials.garage = "garage1"
    var invitation_info = this.navParams.get('passed_from_Invitation')
    this.registerCredentials.email = invitation_info.email;
    this.registerCredentials.firstName = invitation_info.firstname;
    this.registerCredentials.lastName = invitation_info.lastname;
    this.registerCredentials.building = invitation_info.building;
    this.registerCredentials.unit = invitation_info.unit;
    this.registerCredentials.spot = invitation_info.spot;
    this.signup_good();
  }

  save() {
    console.log(this.signUpForm.controls);
    this.submitAttempt = true;


    if (!this.signUpForm.valid) {
      //Something was wrong
      let debug = new DebugMessage("unknown", "signin", JSON.stringify(this.registerCredentials), moment().toLocaleString(), "invalid form");
      if (debug.debugOnOff) {
        this.database.update_DB_childByParent("/debug/", this.registerCredentials.email.replace(/\./g,","), moment().unix().toString(), debug);
      }

      if (this.debugMode) {
        console.log("invalid");
        console.log(debug);
        console.log(this.signUpForm);
      }

    }
    if (this.signUpForm.valid) {
      if (this.debugMode) {
        console.log("success!")
        console.log(this.signUpForm.value);
        console.log(this.registerCredentials);
      }
      let debug = new DebugMessage("unknown", "signin", JSON.stringify(this.registerCredentials), moment().toLocaleString(), "valid form filled");
      if (debug.debugOnOff) {

        this.database.update_DB_childByParent("/debug/", this.registerCredentials.email.replace(/\./g,","), moment().unix().toString(), debug);
      }
      this.register();

    }
  } // end of Save()

  public register() {
    //register with Ionic

    this.showLoading("Attempting to contact the server...");


    setTimeout(() => {
      if (this.badconnection) {
        this.loading.dismiss();
        this.showPopup('Unable to reach server', "We were unable to create an account. Please check your internet connection and try again.")
      }

    }, 10000);


    this.authprovider.register({ email: this.registerCredentials.email, password: this.registerCredentials.password }).then((result) => {

      this.loading.dismiss();
      this.badconnection = false;

      this.createSuccess = true;

      this.showPopup("Success", "Account created.");
      // user, page, info, datetime (in local time), comment

      let debug = new DebugMessage(this.registerCredentials.email, "signin", JSON.stringify(this.registerCredentials), moment().toLocaleString(), "successful account creation");
      if (debug.debugOnOff) {
        this.database.update_DB_childByParent("/debug/", this.registerCredentials.email.replace(/\./g,","), moment().unix().toString(), debug);
      }

      //put user info in the firebase database ONLY UPON SUCCESS

      const items = this.af.database.list('/users/');

      items.update(
        this.registerCredentials.email.replace(/\./g,","), {
        firstName: this.registerCredentials.firstName,
        lastName: this.registerCredentials.lastName,
        password: this.registerCredentials.password,
        building: this.registerCredentials.building,
        garage: this.registerCredentials.garage,
        spot: this.registerCredentials.spot,
        unit: this.registerCredentials.unit,
        insurance: this.registerCredentials.insurance,
        policy: this.registerCredentials.policy,
        make_model: this.registerCredentials.make_model,
        body: this.registerCredentials.body,
        color: this.registerCredentials.color,
        plateState: this.registerCredentials.plateState,
        plate: this.registerCredentials.plate,
        telephone: this.registerCredentials.telephone,
        badge: 0
      });

      //put spot info in the firebase database. This is where offers will be shown when they are made by the user.

      const spots = this.af.database.list('/buildings/' + this.registerCredentials.building + '/' + this.registerCredentials.garage + '/spots/');

      if (this.registerCredentials.spot != "") {
        spots.update("spot_" + this.registerCredentials.spot,
          {
            owneruser: this.registerCredentials.email,
            global_spot_status: 1,
          });
      }



    }).catch((err) => {

      if (this.debugMode) {
        console.log("error registering", err);
      }

      this.loading.dismiss();
      this.badconnection = false;

      this.showPopup("Error", "Problem creating account.  Error details: " + err.toString());

    });

  }//end of Register

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.push(LoginPage);  //this.nav.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  } //end of showPopup


  /**
  * Nothing here....
  */
  showLoading(message: string): void {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.present();
  }

  signup_good() {
    let alert = this.alertCtrl.create({
      title: "Invitation Redeemed!:",
      subTitle: '',
      cssClass: 'custom-alert-happy',
      message: `<p>You've successfully redeemed the invitation code!</p>
      <p>Please complete the information on the signup sheet to create your account.</p>
      <img src="assets/img/sun.png" style="width: 50%; height: 50%"/>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

}



