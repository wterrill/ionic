import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GenericDatabase } from '../../providers/generic-database';

/*
  Generated class for the Query page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-query',
  templateUrl: 'query.html'
})
export class QueryPage {
  starttime: Number=0;
  endtime: Number=0;
  value: any;
  value2: any;
  debugMode: boolean = true;
  startpromise: any;
  endpromise: any;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private database: GenericDatabase) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad QueryPage');
  }

  query(){
    console.log("query entry");
    console.log(typeof(this.starttime));
    //Start time query
      this.startpromise = this.database.queryDB_table_byChildEqual("spots/spot_253/offers","offer_start",Number(this.starttime)); 
      this.startpromise.subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.value = JSON.stringify(queryResultSet[0]);
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });

    //End time query
      this.endpromise = this.database.queryDB_table_byChildEqual("spots/spot_253/offers","offer_end",Number(this.endtime)); 
      this.endpromise.subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.value2 = JSON.stringify(queryResultSet[0]);
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
  }

}
