import { Component, ViewChild } from '@angular/core'; //Pipe, Injectable
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Http } from '@angular/http'; //Headers
import { MessagesDatabase } from '../../providers/messages-database'
import 'rxjs/add/operator/map';
import { WindowService } from '../../providers/window-service';
import { Content } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification-provider';


@Component({
  selector: 'page-message-modal',
  templateUrl: 'message-modal.html',
})


export class MessageModalPage {

  listItems: {
    username: String,
    image: String,
    id: any
  }

  messageList: {
    fromUser: any,
    dateTime: any,
    message: any
  }[] = new Array();

  reverseList: {
    fromUser: any,
    dateTime: any,
    message: any
  }[] = new Array();

  chatId: string;
  msg: String;
  currentUser: string;
  target: any;
  initialLoadDone: boolean = true;

  private debugMode: boolean = false;

  @ViewChild(Content) content: Content;


  ///////////////////////////////////////////////////CONSTRUCTOR//////////////////////////////////////////
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public http: Http, private messageDb: MessagesDatabase, private windowService: WindowService,
    private notificationProvider: NotificationProvider) {

    if (this.debugMode) {
      console.log("NAVPARAMS:", navParams);
    }
    this.listItems = { username: navParams.data.user, image: "", id: "" };
    if (this.debugMode) {
      console.log("navParams: ", navParams.data.chatId);
    }
    this.chatId = navParams.data.chatId.toString();
    this.currentUser = this.windowService.getKey('user', '');
    if (this.debugMode) {
      console.log(this.chatId);
    }
    this.initializeMessages();
    console.log("entering message-modal page");
  }


  ionViewDidEnter() {
    console.log("MessageModalPage loaded");
    this.content.scrollToBottom();
  }



  ///////////////////////////////////////////////////INITIALIZEMESSAGES//////////////////////////////////////////
  // Sets the database to the proper collection
  // and subscribes to the messages-database
  // observable stream.  This gives a real time update
  // of messages in the database.

  initializeMessages() {
    this.messageDb.getMessages(this.chatId, 40);

    this.messageDb.messageStream.map(response => {
      if (this.debugMode) {
        console.log("response: ", response);
      }
      return response;

    }).subscribe(messageList => {
      this.messageList = messageList;
      if (this.debugMode) {
        console.log("The message list from inside the modal");
        console.log(this.messageList);
      }
      this.target = (JSON.parse(JSON.stringify(this.messageList)));
      // console.log("this.target value before = ", this.target);
      // this.target.replace(/(\r\n|\n\r|\r|\n)/gm, "<br>");
      // console.log("this.target value after = ", this.target);

      var that = this //well, now, this is interesting: https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setTimeout#The_this_problem
      setTimeout(function () { that.content.scrollToBottom(500); }, 300); //Man, I don't like doing this... but I think it scrolls all the way down before the latest message comes in.
    });
  }


  ///////////////////////////////////////////////////TRANSMITMSG////////////////////////////////////////
  transmitMsg() {
    if (this.msg != "") { //no need to send out a message if it's blank.
    let data = ""

      // send notification thru provider
      this.notificationProvider.sendNotification(this.listItems.username.toString(), "New message from " + this.currentUser
        + ": " + this.msg, "chat", data);

      // post message to database
      this.messageDb.sendMessageFirebase(this.msg);
      this.msg = ""; //clear message after it is sent
      this.content.scrollToBottom(300); //make sure you're scrolled to the bottom of the messages.

    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
    this.initialLoadDone = true;
  }

}




