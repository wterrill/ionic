import { Component, trigger, state, style, transition, animate, keyframes, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TweenMax, Cubic } from 'gsap';




//this code is from the angular 2 
// from this tutorial https://teropa.info/blog/2016/12/12/graphics-in-angular-2.html
// had to do : npm install -S gsap @types/greensock for greensock
/*
  Generated class for the Svgmap page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-svgmap',
  templateUrl: 'svgmap.html',
  animations: [
    trigger('aState', [
      state('small', style({transform: 'scale(1)'})),
      state('large', style({transform: 'scale(1.5)'})), 
      transition('small => large', animate('1s ease', keyframes([
        style({transform: 'scale(1)', offset: 0}),
        style({transform: 'scale(0.7) rotate(15deg)', offset: 0.15}), //offset 0.15
        style({transform: 'scale(1)', offset: 0.3}),  //0.3
        style({transform: 'scale(1.5)', offset: 1})   //offset 1   //scale(4.2)
      ]))),
      transition('large => small', animate('1s ease', keyframes([
        style({transform: 'scale(1.5)', offset: 0}),
        style({transform: 'scale(5) rotate(-15deg)', offset: 0.15}), //offset 0.15
        style({transform: 'scale(1.5)', offset: 0.3}), //0.3
        style({transform: 'scale(1)', offset: 1}) //offset 1
      ])))
    ])
  ]
})












export class SvgmapPage implements OnInit {
  public centerx: number = 100;
  public centery: number = 100;
  public aState: string = "small"

  @ViewChild('left') left: ElementRef;
  @ViewChild('right') right: ElementRef;

  

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SvgmapPage');
  }

  ngOnInit() {
    TweenMax.to(this.left.nativeElement, 1, {
      attr: {
        points: '125,30 125,30 125,30 31.9,30 31.9,230 125,230 125,230 125,230 203.9,186.3 218.1,63.2'
      },
      repeat: -1,
      yoyo: true,
      ease: Cubic.easeInOut
    });
    TweenMax.to(this.right.nativeElement, 1, {
      attr: {
        points: '125,30 125,52.2 125,52.1 125,153.4 125,153.4 125,230 125,230 218.1,230 218.1,30 125,30'
      },
      repeat: -1,
      yoyo: true,
      ease: Cubic.easeInOut
    });
  }

  setCircleLocation(e){
    console.log("made it");
    console.log(e.center.x,e.center.y);  
    this.centerx = e.center.x - 60; // the 50 is adjustment to get center of circle.
    this.centery = e.center.y - 150; //the 450 was due to the fact that you have to scroll down so far to see this value.
  }

  toggleAState(){
    console.log("toggled");
    if (this.aState==="small")
    {
      this.aState = "large";
    }
    else
    {
      this.aState = "small";
    }


  }
}
