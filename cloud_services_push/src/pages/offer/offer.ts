import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GenericDatabase } from '../../providers/generic-database';
import { WindowService } from '../../providers/window-service';
import { AuthService } from '../../providers/auth-service';
import { DatastreamService } from '../../providers/datastream-service';
import { AlertController } from 'ionic-angular';
import { LandingPage } from '../landing/landing';
import { OfferHandler } from '../../components/offerhandler';
import { OfferPendingReservedPage } from '../offerPendingReserved/offerPendingReserved';
import * as moment from 'moment';
import { Transaction } from '../../components/library';
import { ToastController } from 'ionic-angular';
import { UserInfoPage } from '../userinfo/userinfo'


/*
  Generated class for the Offer page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html'
})
export class OfferPage {

  public event = {
    startDate: "",
    endDate: '2050-06-05',
    startTime: '07:45',
    endTime: '08:00'
  }

  private offeredSpotsStream$ = new Array();

  currentDate: Date;
  pickerStartDate: String;
  pickerEndDate: String;
  startDate: Date;
  endDate: Date;
  localDate: string;
  stringDate: string;
  str_startingDT: string;
  str_endingDT: string;
  dat_startingDT: Date;
  dat_endingDT: Date;
  startingMsec: number;
  endingMsec: number;
  str_timeStartEntered: string;
  str_timeEndEntered: string;
  temp1: any; //for debugging
  temp2: any; //for debugging
  gotspots: boolean = false;

  private num_garages: number;

  private debugMode: boolean = true;




  /**
   * offer page constructor
   * @param navCtrl 
   * @param navParams 
   * @param gdb 
   * @param windowService 
   * @param datastream 
   * @param alertCtrl 
   */

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public gdb: GenericDatabase, 
    private auth: AuthService,
    public windowService: WindowService, 
    public datastream: DatastreamService, 
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private database: GenericDatabase) {

    this.currentDate = new Date();
    this.localDate = this.currentDate.toLocaleString();

    this.startDate = new Date(this.currentDate.getTime());
    this.startDate.setHours(this.currentDate.getHours() + 1);

    this.event.startDate = this.startDate.getFullYear() + "-" +
      ("0" + (this.startDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.startDate.getDate())).slice(-2);

    this.endDate = new Date(this.currentDate.getTime());
    this.endDate.setHours(this.currentDate.getHours() + 2);

    if (this.debugMode) {
      console.log("currentDate");
      console.log(this.currentDate);
      console.log("startDate");
      console.log(this.startDate);
      console.log("endDate");
      console.log(this.endDate);
    }

    this.event.endDate = this.endDate.getFullYear() + "-" +
      ("0" + (this.endDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.endDate.getDate())).slice(-2);

    this.event.startTime = ("0" + this.startDate.getHours()).slice(-2) + ":00";
    this.event.endTime = ("0" + this.endDate.getHours()).slice(-2) + ":00";

    console.log("entering offer page");
    let temp = moment(this.currentDate);
    let temp2 = moment(this.currentDate).add(3, 'M');
    this.pickerStartDate = temp.format('YYYY-MM-DD');
    this.pickerEndDate = temp2.format('YYYY-MM-DD');
    this.gotspots = this.queryownspots();

    this.num_garages = parseInt(this.windowService.getKey("num_garages","1"));
  }




  /**
   * Generic code called by ionic when the page loaded.
   */

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferPage');
  }

  /**
   * Method to reset the starting date time
   * when an offer is in valid or the user
   * wants to make a new offer.
   */

  private redo() {
    this.str_startingDT = "";
  }

  /**
   * Method that sends the actual offer to the database
   * once the user has confirmed that they wish to push
   * or make the space available for the given time.
   */

  private send() {

    var newOffer = new Transaction(this.str_timeStartEntered, this.str_timeEndEntered, ' ', ' ', this.windowService.getKey('building', ''),
      this.windowService.getKey('garage', ''), this.endingMsec, this.startingMsec, 1, this.endingMsec - this.startingMsec,
      this.windowService.getKey("user", ''), -1, -1, ' ', -1, 'spot_' + this.windowService.getKey('spot', ''));

    if (this.debugMode) {
      console.log('This is what an object type offer looks like', newOffer);
    }

    var spot = new Array();

    spot.push({
      building: this.windowService.getKey('building', ''),
      garage: this.windowService.getKey('garage', ''),
      spot: 'spot_' + this.windowService.getKey('spot', ''),
      offer_start: this.startingMsec.toString(),
      offer_end: this.endingMsec.toString()
    });

    if (this.debugMode) {
      console.log('Just checking the spot pre offerCheck in offer.ts, spot: ', spot)
    }

    let offerHandler = new OfferHandler(this.alertCtrl, this.gdb, this.auth);

    // begin the process of checking to see if the offer already exists.
    // this is accomplished with an offerHandler promise.  
    // use .then({}) to define the synchronous code that occurs when you 
    // are able to offer the spot
    // and use .catch({}) to define the syncrhonous code that occurs when
    // the spot was already offered at the desired starting time.

    offerHandler.offerCheck(spot[0]).then(response => {


      offerHandler.createOffer(newOffer, 1);
      this.showThanks();

    }).catch(() => {

      let startingDT = moment(this.str_startingDT);
      let endingDT = moment(this.str_endingDT);
      startingDT.format('ddd MMM Do YYYY')
      endingDT.format('ddd MMM Do YYYY')
      let alert = this.alertCtrl.create({
        // tried to add custom css to the
        // alert using cssClass option,
        // does not appear to work.
        cssClass: 'custom-alert-alert',
        title: 'Oops, something went wrong!',
        message: `<p style="color:d81000">It appears your spot is already available sometime between:
                <br>
                <br>
                <b>` + startingDT.format('ddd MMM Do YYYY, LT') + `</b>
                <br><br> and <br><br>
                <b>` + endingDT.format('ddd MMM Do YYYY, LT') + `</b>
                <br>
                <br>
                Please check your current offers and try again</p>`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Disagree clicked');
              this.redo();
            }
          }]
      });

      alert.present();

    });
  }


  /**
   * Queries the database to find the spots associated with the user
   * that are currently made available.
   */

  queryownspots() {

    //let totalAvailableSpots = 0;

    this.offeredSpotsStream$ = new Array();

    this.datastream.offeredSpotsStream$.subscribe(filteredSpots => {
      console.log("The spots after mapping the dataStream are: ");
      console.log(filteredSpots);
      this.offeredSpotsStream$ = filteredSpots;
    }, err => {
      console.log("Error with the offers data stream");
      console.log(err);
    })
    if (this.offeredSpotsStream$.length > 0) {
      return true;
    } else {
      return false;
    }
  }
  //end of queryownspots


  /**
   * Deletes a specified offer from the database and makes the
   * spot unavailable at that time.
   * 
   * @param value The spot / offer that wishes to be deleted.
   */

  private delete(value) {

    if(this.debugMode) {
      console.log('offers.ts is attempting to delete the value', value);
    }

    var path = 'buildings/' + value.building + '/' + value.garage + '/spots/' + value.spot + '/offers/offer_' + value.offer_start;

    this.gdb.deleteRecord(path);
    if (this.debugMode) {
      console.log("delete information")
      console.log(value)
      console.log(this.offeredSpotsStream$)
      this.gotspots = this.queryownspots();
    }
  }


  /**
   * Checks the values entered by the user to ensure
   * that the start and end times are correct (Do not overlap)
   * 
   */

  public checkvalues() {
    //let's get the data into a format that makes sense
    let hoursoffset = this.currentDate.getTimezoneOffset() / 60;
    let str_hoursoffset = ("0" + Math.abs(hoursoffset)).slice(-2) + ":00";
    let str_houroffsetSign = "-";
    //hoursoffset = time in minutes between GMT and localetime = GMT - Localtime.  
    //Therefore if offset is positive, you subtract hours to get local timezone.
    //If the sign is negative. localtime is ahead of GMT.
    if (hoursoffset < 0) {
      str_houroffsetSign = "+";
    }
    this.str_startingDT = this.event.startDate + "T" + this.event.startTime + ":00" + str_houroffsetSign + str_hoursoffset;
    this.str_endingDT = this.event.endDate + "T" + this.event.endTime + ":00" + str_houroffsetSign + str_hoursoffset;
    this.dat_startingDT = new Date(this.str_startingDT);
    this.dat_endingDT = new Date(this.str_endingDT);
    this.startingMsec = this.dat_startingDT.getTime();
    this.endingMsec = this.dat_endingDT.getTime();
    this.str_timeStartEntered = this.event.startDate + " " + this.event.startTime
    this.str_timeEndEntered = this.event.endDate + " " + this.event.endTime

    //do initial checks... is start time before end time?  is start time in the future?
    if (this.startingMsec < this.endingMsec &&
      this.startingMsec > this.currentDate.getMilliseconds()) {
      //If this is true, then show the pop-up to confirm.
      this.showConfirm();
    } else {
      this.showError();
    }

    if (this.debugMode) {
      console.log("timezone offset is: ", this.currentDate.getTimezoneOffset);
      console.log("str_startingDT, str_endingDT:");
      console.log(this.str_startingDT);
      console.log(this.str_endingDT);
    }
  }






  /**
   * Shows the confirmation dialog before making a spot available
   * in the database.  User must confirm or redo their
   * offer.
   */

  private showConfirm() {
    let startingDT = moment(this.str_startingDT);
    let endingDT = moment(this.str_endingDT);
    startingDT.format('ddd MMM Do YYYY')
    let confirm = this.alertCtrl.create({
      cssClass: 'custom-alert-check',
      title: 'Check the dates:',
      message: `<p>Would you like to offer your parking space from:</p>
                <b><p>` + startingDT.format('ddd MMM Do YYYY, LT') + `</p></b>
                <p>Until:</p>
                <b><p>`+ endingDT.format('ddd MMM Do YYYY, LT') + `?</p><b>`,
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
            this.redo();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.send();

          }
        }
      ]
    });
    confirm.present();
  }







  /**
   * Creates an alert controller that says thank you
   * to the user once they have confirmed an offer and
   * made their spot available.
   * 
   */
  private showThanks() {
    let startingDT = moment(this.str_startingDT);
    let endingDT = moment(this.str_endingDT);
    startingDT.format('ddd MMM Do YYYY')
    let confirm = this.alertCtrl.create({
      title: 'You are AWESOME!',
      cssClass: "custom-alert-happy",
      message: `<p>You've just offered your parking spot from:</p>
                <b><p>` + startingDT.format('ddd MMM Do YYYY, LT') + `</p></b>
                <p>Until:</p>
                <b><p>`+ endingDT.format('ddd MMM Do YYYY, LT') + `</p></b>
                <p> and it is very much appreciated!</p>
                <p></p>
                <p>Would you like offer another timeframe?`,
      buttons: [
        {
          text: 'No',
          handler: () => {
            this.navCtrl.push(LandingPage);
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.gotspots = this.queryownspots();

          }
        }
      ]
    });
    confirm.present();
  }





  /**
   * 
   * Currently unimplemented.
   * 
   * @param spot the spot the user wishes to delete
   */

  public deleteConfirm(spot) {
    let confirm = this.alertCtrl.create({
      title: 'Deleting an offer:',
      cssClass: 'custom-alert-check',
      message: 'Are you sure you want to delete this offer for your parking spot?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            //this.navCtrl.push(LandingPage);
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.delete(spot);
          }
        }
      ]
    });
    confirm.present();
  }

  /**
   * Generic error dialog shown to the user
   * when and offer fails for one reason
   * or another (database connectivity or
   * start / end times are out of whack)
   * 
   */

  showError() {
    let confirm = this.alertCtrl.create({
      title: 'Data Error',
      cssClass: 'custom-alert-danger',
      message: "There's something off about your dates.  Like... maybe your end time is before your start time? Or maybe both are sometime in the past? Let's give it another go. ",
      buttons: ["OK"]
    });
    confirm.present();
  }

  gotoOfferPendingPage() {
    this.navCtrl.push(OfferPendingReservedPage);
  }

  presentToast(user){
        this.database.queryDB_single_item_as_Promise("users/",user.replace(/\./g, ","))
      .then((x)=>{

        var result = x.reduce(function(map, obj) {
        map[obj.$key] = obj.$value;
          return map;
        }, {});
        console.log("*****result*****")
        console.log(result)


        var car_body = (result.body == "4sedan" ? "4-Door Sedan" : result.body); 
        car_body = (car_body == "2sedan" ? "2-Door Coupe" : car_body);
        car_body = (car_body == "Wagon" ? "Station Wagon" : car_body);
        car_body = (car_body == "SportsCar" ? "Sports Car" : car_body);

        var message = "First Name: " + result["firstName"].charAt(0).toUpperCase() + result["firstName"].slice(1) + "\n" +
        "Last Name: " + result["lastName"].charAt(0).toUpperCase() + result["lastName"].slice(1) + "\n" +
        "Unit Number: " + result.unit + "\n" +
        "Phone Number: " + result.telephone + "\n" +
        "Car Make/Model: " + result.make_model + "\n" +
        "Body Style: " + car_body  + "\n" +
        "Car Color: " + result.color  + "\n" +
        "License Plate State: " + result.plateState  + "\n" +
        "License Plate Number: " + result.plate  + "\n" +
        "Insurance Company: " + result.insurance

        console.log(message);
        this.the_Toast(message);

      })
  }

  the_Toast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      //duration: 3000,
      showCloseButton: true,
      closeButtonText: "Close"
    });
    toast.present();
  }


  
  userpage(user){
    this.navCtrl.push(UserInfoPage,user);
  }

}
