import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NavController, NavParams } from 'ionic-angular';
import { WindowService } from '../../providers/window-service';
import { GenericDatabase } from '../../providers/generic-database'
import { SpotValidator } from '../../validators/spotValidator';
import { SpotExistsValidator } from '../../validators/spotExistsValidator';
import { AlertController } from "ionic-angular";
import { LandingPage } from '../landing/landing';

/*
  Generated class for the Management page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-management',
  templateUrl: 'management.html'
})
export class ManagementPage {

  private debugMode: boolean = true;

  private currentCredentials = {
    email: this.windowService.getKey('user', ''),
    firstName: this.windowService.getKey('firstName', ''),
    lastName: this.windowService.getKey('lastName', ''),
    building: this.windowService.getKey('building', ''),
    garage: this.windowService.getKey('garage', ''),
    spot: this.windowService.getKey('spot', ''),
    unit: this.windowService.getKey('unit', ''),
    telephone: this.windowService.getKey('telephone', ''),
    insurance: this.windowService.getKey('insurance', ''),
    policy: this.windowService.getKey('policy', ''),
    make_model: this.windowService.getKey('make_model', ''),
    body: this.windowService.getKey('body', ''),
    color: this.windowService.getKey('color', ''),
    plateState: this.windowService.getKey('plateState', ''),
    plate: this.windowService.getKey('plate', '')
  };

  private updateCredentials = {
    firstName: '',
    lastName: '',
    unit: '',
    telephone: '',
    insurance: '',   
    policy: '',
    make_model: '',
    body: '',
    color: '',
    plateState: '',
    plate: ''
  };

  private editForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private database: GenericDatabase,
    private spotExistsValidator: SpotExistsValidator,
    private formBuilder: FormBuilder,
    private windowService: WindowService,
    private alertCtrl: AlertController
  ) {

    this.populateForm();
    if (this.debugMode) {
      console.log("entering management page, ", this.currentCredentials);
    }
  }

  private populateForm(): void {
    this.editForm = this.formBuilder.group({
      email: [this.currentCredentials.email, Validators.compose([Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'), Validators.required])],
      firstName: [this.currentCredentials.firstName, Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: [this.currentCredentials.lastName, Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      spot: [this.currentCredentials.spot, [SpotValidator.isValid], this.spotExistsValidator.checkSpotExists.bind(this.spotExistsValidator)],
      unit: [this.currentCredentials.unit, Validators.required],
      telephone: [this.currentCredentials.telephone],
      insurance: [this.currentCredentials.insurance],
      policy: [this.currentCredentials.policy],
      make_model: [this.currentCredentials.make_model, Validators.required],
      body: [this.currentCredentials.body, Validators.required],
      color: [this.currentCredentials.color, Validators.required],
      plateState: [this.currentCredentials.plateState, Validators.required],
      plate: [this.currentCredentials.plate, Validators.required]
    });
  }

  private updateForm(): void {
    this.editForm.setValue({
      email: [this.currentCredentials.email],
      firstName: [this.currentCredentials.firstName],
      lastName: [this.currentCredentials.lastName],
      spot: [this.currentCredentials.spot],
      unit: [this.currentCredentials.unit],
      telephone: [this.currentCredentials.telephone],
      insurance: [this.currentCredentials.insurance],
      policy: [this.currentCredentials.policy],
      make_model: [this.currentCredentials.make_model],
      body: [this.currentCredentials.body],
      color: [this.currentCredentials.color],
      plateState: [this.currentCredentials.plateState],
      plate: [this.currentCredentials.plate]
    });
  }

  public save() {

    this.updateCredentials.firstName = this.currentCredentials.firstName;
    this.updateCredentials.lastName = this.currentCredentials.lastName;
    this.updateCredentials.unit = this.currentCredentials.unit;
    this.updateCredentials.telephone = this.currentCredentials.telephone;
    this.updateCredentials.insurance = this.currentCredentials.insurance;
    this.updateCredentials.policy = this.currentCredentials.policy;
    this.updateCredentials.make_model = this.currentCredentials.make_model;
    this.updateCredentials.body = this.currentCredentials.body;
    this.updateCredentials.color = this.currentCredentials.color;
    this.updateCredentials.plateState = this.currentCredentials.plateState;
    this.updateCredentials.plate = this.currentCredentials.plate;

    var success = this.database.update_DB_childByParent('/', "users", this.currentCredentials.email.replace(/\./g, ","), this.updateCredentials)
    if(success){
      this.saved_success_message();
      this.windowService.setKey('firstName', this.currentCredentials.firstName),
      this.windowService.setKey('lastName', this.currentCredentials.lastName),
      this.windowService.setKey('unit', this.currentCredentials.unit),
      this.windowService.setKey('telephone', this.currentCredentials.telephone),
      this.windowService.setKey('insurance', this.currentCredentials.insurance),
      this.windowService.setKey('policy', this.currentCredentials.policy),
      this.windowService.setKey('make_model', this.currentCredentials.make_model),
      this.windowService.setKey('body', this.currentCredentials.body),
      this.windowService.setKey('color', this.currentCredentials.color),
      this.windowService.setKey('plateState', this.currentCredentials.plateState),
      this.windowService.setKey('plate', this.currentCredentials.plate)
    } else {
      this.saved_failed_message();
    }


  }//end of save

  saved_success_message() {
    let alert = this.alertCtrl.create({
      title: "You've updated your info",
      subTitle: '',
      cssClass: 'custom-alert-happy',
      message: `<p>Your account information has been successfully updated.</p>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            this.navCtrl.push(LandingPage);
          }
        }
      ]
    });
    alert.present();
  }

  saved_failed_message() {
    let alert = this.alertCtrl.create({
      title: "Something happened :/",
      subTitle: '',
      cssClass: 'custom-alert-danger',
      message: `<p>Your account information was not updated.</p>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


}
