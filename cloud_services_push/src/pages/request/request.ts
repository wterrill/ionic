import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { GenericDatabase } from '../../providers/generic-database';
import { DatastreamService } from '../../providers/datastream-service';
import { WindowService } from '../../providers/window-service';
import { NotificationProvider } from '../../providers/notification-provider';
import { AuthService} from '../../providers/auth-service';
import { AlertController } from "ionic-angular";
import { RequestHandler } from '../../components/requesthandler';
import { Transaction } from '../../components/library';
import * as moment from 'moment';


@Component({
  selector: 'page-request',
  templateUrl: 'request.html'
})
export class RequestPage {

  private num_garages: number;
  private loading: Loading;
  private currentUser: string;
  private debugMode: boolean = false;
  private totalAvailableSpaces: number = 0;
  private invalidSearch: boolean = false;

  private event = {
    startDate: "",
    endDate: '2050-06-05',
    startTime: '07:45',
    endTime: '08:00'
  };

  private currentDate: Date;
  private pickerStartDate: String;
  private pickerEndDate: String
  private startDate: Date;
  private endDate: Date;
  private localDate: string;
  private str_startingDT: string;
  private str_endingDT: string;
  private dat_startingDT: any;
  private dat_endingDT: any;
  private startingMsec: number;
  private endingMsec: number;
  private validRequest: Boolean;
  private garage: String = "";
  //private spot: String

  private availableSpotsStream$: {
    spot: String,
    _1_offer_start: String,
    _2_offer_end: String,
    weight: number,
    offer_status: number
  }[];



  constructor( public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
               private database: GenericDatabase, private datastream: DatastreamService, private windowService: WindowService,
               private notificationProvider: NotificationProvider, private alertCtrl: AlertController, private auth: AuthService) {

    this.currentUser = this.windowService.getKey('user', '');
    this.reset();



    //added by will to fix midnight date error___________________

    this.currentDate = new Date();
    this.localDate = this.currentDate.toLocaleString();

    this.startDate = new Date(this.currentDate.getTime());
    this.startDate.setHours(this.currentDate.getHours() + 1);
    this.startingMsec = this.startDate.getTime();

    this.event.startDate = this.startDate.getFullYear() + "-" +
      ("0" + (this.startDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.startDate.getDate())).slice(-2);

    this.endDate = new Date(this.currentDate.getTime());
    this.endDate.setHours(this.currentDate.getHours() + 2);
    this.endingMsec = this.endDate.getTime();

    if (this.debugMode) {
      console.log("currentDate");
      console.log(this.currentDate);
      console.log("startDate");
      console.log(this.startDate);
      console.log("endDate");
      console.log(this.endDate);
    }

    this.event.endDate = this.endDate.getFullYear() + "-" +
      ("0" + (this.endDate.getMonth() + 1)).slice(-2) + "-" +
      ("0" + (this.endDate.getDate())).slice(-2);

    this.event.startTime = ("0" + this.startDate.getHours()).slice(-2) + ":00";
    this.event.endTime = ("0" + this.endDate.getHours()).slice(-2) + ":00";

    let temp = moment(this.currentDate);
    let temp2 = moment(this.currentDate).add(3, 'M');
    this.pickerStartDate = temp.format('YYYY-MM-DD');
    this.pickerEndDate = temp2.format('YYYY-MM-DD');

    console.log("entering request page");

    this.num_garages = parseInt(this.windowService.getKey("num_garages","1"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestPage');
  }



  /** #########################################################
   * ###########################################################
   *              !!!!BEGIN QUERY FUNCTION!!!!
   *  #########################################################
   * ###########################################################
   * 
   * 
   * 
   * This function checks the current request and returns those
   * available spaces that match the desired start / end times.
   * 
   * 
   * If the user entered an invalid request, then no data is returned
   * and the user is prompted to update their selection.
   * 
   */
  querySpaces() {

    this.showLoading("Searching available spots...");

    var totalAvailableSpots = 0;
    var availableSpotsRefined = new Array();
    var finalSpots = new Array();

    this.availableSpotsStream$ = new Array();

    // set the start date / end date to MSEC format.
    this.checkValues();

    if (this.validRequest) {
      this.datastream.availableSpotsStream$.map(response => {

        if (this.debugMode) {
          console.log("This is the datastream provided observable: ");
          console.log(response);
          console.log(response.length);
        }

        for (var j = 0; j < response.length; j++) {
          if (this.debugMode) {
            console.log("start and stop values for loop");
            console.log('response[', j, '].offer_start: ', response[j].offer_start);
            console.log('this.startingMsec: ', this.startingMsec);
            console.log('response[', j, '].offer_end: ', response[j].offer_end);
            console.log('this.endingMsec: ', this.endingMsec);
          }
          if (response[j].offer_start <= this.startingMsec && response[j].offer_end >= this.endingMsec && response[j].offer_status == 1) {

            if (this.debugMode) {
              console.log("Pushing response[" + j + "]");
              console.log(response[j]);
            }

            totalAvailableSpots++;

            availableSpotsRefined.push(
              new Transaction(
              response[j]._1_offer_start, 
              response[j]._2_offer_end,
              response[j]._3_request_start, 
              response[j]._4_request_end,
              response[j].building, 
              response[j].garage,
              response[j].offer_end, 
              response[j].offer_start,
              response[j].offer_status, 
              response[j].offer_weight,
              response[j].owner, 
              response[j].request_end,
              response[j].request_start, 
              response[j].request_user,
              response[j].request_weight, 
              response[j].spot));

          }

          else {
            if (response[0].start <= this.startingMsec && response[0].end >= this.endingMsec) {

              if (this.debugMode) {
                console.log("Pushing response[0]");
                console.log(response);
              }

              totalAvailableSpots++;
              availableSpotsRefined.push(
                new Transaction(
                response[0]._1_offer_start, 
                response[0]._2_offer_end,
                response[0]._3_request_start, 
                response[0]._4_request_end,
                response[0].building, 
                response[0].garage,
                response[0].offer_end, 
                response[0].offer_start,
                response[0].offer_status, 
                response[0].offer_weight,
                response[0].owner, 
                response[0].request_end,
                response[0].request_start, 
                response[0].request_user,
                response[0].request_weight, 
                response[0].spot));

            }
          }


          if (this.debugMode) {
            console.log("Refined Available Spots: ");
            console.log(availableSpotsRefined)
          }
        }
        if(this.garage!=""){
          totalAvailableSpots = 0;
          for (var k = 0; k < availableSpotsRefined.length; k++) {
            if(availableSpotsRefined[k].garage == this.garage){
              finalSpots.push(
              new Transaction(
              availableSpotsRefined[k]._1_offer_start, 
              availableSpotsRefined[k]._2_offer_end,
              availableSpotsRefined[k]._3_request_start, 
              availableSpotsRefined[k]._4_request_end,
              availableSpotsRefined[k].building, 
              availableSpotsRefined[k].garage,
              availableSpotsRefined[k].offer_end, 
              availableSpotsRefined[k].offer_start,
              availableSpotsRefined[k].offer_status, 
              availableSpotsRefined[k].offer_weight,
              availableSpotsRefined[k].owner, 
              availableSpotsRefined[k].request_end,
              availableSpotsRefined[k].request_start, 
              availableSpotsRefined[k].request_user,
              availableSpotsRefined[k].request_weight, 
              availableSpotsRefined[k].spot));
            }
            totalAvailableSpots++;
          }
          if (this.debugMode) {
            console.log("Final Available Spots: ");
            console.log(finalSpots)
          }
          console.log("******************totalAvailableSpace*****")
          console.log(this.totalAvailableSpaces)
          return finalSpots;
        }

        return availableSpotsRefined;

      }).subscribe(filteredSpots => {

        if (this.debugMode) {
          console.log("The spots after mapping the dataStream are: ");
          console.log(filteredSpots);
        }
        setTimeout(() => {
          this.loading.dismiss();
          this.totalAvailableSpaces = totalAvailableSpots;
          this.availableSpotsStream$ = filteredSpots;

          if (this.totalAvailableSpaces < 1) {
            this.invalidSearch = true;
          }

        }, 500);
      }, err => {
        this.loading.dismiss();
        console.log("Error with the offers data stream");
        console.log(err);
      })

    } else {

      this.loading.dismiss();

      let confirm = this.alertCtrl.create({
        title: 'Hmmm...',
        message: 'Something seems off about your request parameters.  Double check your entry and try again.',
        cssClass: 'custom-alert-check',
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Alert dismissed.');
              this.reset();
            }
          }]
      });
      confirm.present();
    }
  }

  /** #########################################################
   * ###########################################################
   *              !!!!BEGIN CHEKC FUNCTION!!!!
   *  #########################################################
   * ###########################################################
   * 
   * This function checks the current request and turns it into
   * Milliseconds (I think).  It will update a private property
   * validRequest if the entry is invalid.
   * 
   */

  checkValues() {
    this.str_startingDT = this.event.startDate + "T" + this.event.startTime + ":00-05:00";
    this.str_endingDT = this.event.endDate + "T" + this.event.endTime + ":00-05:00";
    this.dat_startingDT = moment.utc(this.str_startingDT);
    this.dat_endingDT = moment.utc(this.str_endingDT);
    this.startingMsec = this.dat_startingDT.valueOf();
    this.endingMsec = this.dat_endingDT.valueOf();


    // Checking to see that the request start time is not the same
    // or after the request end time (which is an invalid entry)
    // if it is, we set validRequest to false.  
    if (this.startingMsec >= this.endingMsec) {
      this.validRequest = false;
    } else {
      this.validRequest = true;
    }
  }


  /**
   * Request a spot and remove it from status 1 (available) to 2 (pending).  The
   * owner must approve or deny (inside the offer.ts area).  It will handle further
   * status updates.
   * 
   * @param spot The spot the user is trying to request.  It will contain the pertinent information
   * (owner, times, spot number, etc.) to properly update the database.
   */
  request(spot: any): void {

    if (this.debugMode) {
      console.log("trying to request spot: ", spot);
      console.log("offer_" + spot.offer_start);
    }

    this.showLoading("Building your request...");

    let requestHandler = new RequestHandler(this.database, this.auth, this.alertCtrl);

    requestHandler.request(spot, this.startingMsec, this.endingMsec).then(data => {

      let start: String = moment.utc(this.startingMsec).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
      let end: String = moment.utc(this.endingMsec).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
      var notificationMessage = this.currentUser.split('@')[0] + " made a park request! \n"
        + start
        + "\nTo: \n"
        + end

      var htmlMessage = `<p>` + this.currentUser.split('@')[0] + " made a park request! </p>" +
        `<p>`+ start + `</p>` + 
        `<p> To: </p>` +
        `<p>`+ end + `</p>`
        

      // update the offer in the database
      /*spot._3_request_start = moment.utc(this.startingMsec).local().toString();
      spot._4_request_end = moment.utc(this.endingMsec).local().toString();
      spot.offer_status = 2;
      spot.request_user = this.currentUser;
      spot.request_start = this.startingMsec;
      spot.request_end = this.endingMsec;
      spot.request_weight = this.endingMsec - this.startingMsec;

      this.database.update_DB_childByParent('/buildings/' + spot.building + '/' + spot.garage + "/spots/" + spot.spot, "/offers", "offer_" + spot.offer_start, spot);*/

      /*this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "_3_request_start", moment.utc(this.startingMsec).local().toString());
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "_4_request_end", moment.utc(this.endingMsec).local().toString());
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "offer_status", 2);
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "request_user", this.currentUser);
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "request_start", this.startingMsec);
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "request_end", this.endingMsec);
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "request_weight", this.endingMsec - this.startingMsec);
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "spot", spot.spot);
      this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers", "offer_" + spot.offer_start, "owner", spot.owner);*/

      var payload = { 
        "spot": spot,
        "htmlMessage": htmlMessage
      }

      this.notificationProvider.sendNotification(spot.owner, notificationMessage, "request", payload);

      setTimeout(() => {
        this.loading.dismiss();
        this.querySpaces();
      }, 500);



    }).catch(() => {

      setTimeout(() => {
        this.loading.dismiss();
      }, 250);

      let confirm = this.alertCtrl.create({
        title: 'Sorry, that space was just taken!',
        message: 'We can\'t seem to offer you that spot anymore.  Searching for available spaces...',
        cssClass: 'custom-alert-check',
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Disagree clicked');
              this.querySpaces();
            }
          }]
      });
      confirm.present();
    });

    this.about();

  }


  /**
   * Nothing here....
   */
  showLoading(message: string): void {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.present();
  }


  /**
   * Resets the number of total available spaces to adjust
   * the HTML view of the Request page.
   */
  reset(): void {
    this.totalAvailableSpaces = 0;
    this.invalidSearch = false;
  }

  sleep(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  about() {
    let alert = this.alertCtrl.create({
      title: "You've Requested a spot!:",
      subTitle: '',
      cssClass: 'custom-alert-happy',
      message: `<p>You've requested a spot from someone in your building.</p>
      <p>They will have to approve your request, unless you're on their white list.</p>
      <p>Check "Requests you've made" after they've had a chance to approve you!</p>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

}
