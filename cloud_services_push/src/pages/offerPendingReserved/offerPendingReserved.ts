import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
//import { Auth, User, UserDetails, IDetailedError, Push, PushToken } from '@ionic/cloud-angular';
import { WindowService } from '../../providers/window-service';
import { AuthService } from '../../providers/auth-service';
//import { ManagementPage } from '../management/management';
//import { OfferPage } from '../offer/offer';
//import { RequestPage } from '../request/request';
//import { SettingsPage } from '../settings/settings';
//import { LoginPage } from '../login/login';
import { GenericDatabase } from '../../providers/generic-database';
import { DatastreamService } from '../../providers/datastream-service';
import { AlertController } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification-provider';
import { Badge } from '@ionic-native/badge';
import { ReservationHandler } from '../../components/reservationhandler';
import * as moment from 'moment';
import { UserInfoPage } from '../userinfo/userinfo'

//import { Events } from 'ionic/angular2';

/*
  Generated class for the Landing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-offerPendingReseved',
  templateUrl: 'offerPendingReserved.html'
})
export class OfferPendingReservedPage {

  username: String;
  private debugMode: boolean = true;
  private totalPendingSpaces: number = 0;
  private totalReservedSpaces: number = 0;

  private pendingSpotsStream$: any[]
  private reservedSpotsStream$: any[]
  private ionicServe: boolean = false;

  private num_garages: number;


  constructor(public navCtrl: NavController, public navParams: NavParams, private windowService: WindowService,
    private datastream: DatastreamService, private alertCtrl: AlertController, private notificationProvider: NotificationProvider,
    private database: GenericDatabase, private badge: Badge, private platform: Platform, private auth: AuthService) {


    if (!this.platform.is("cordova")) {
      if (this.debugMode) {
        console.log("running ionic serve");
      }
      this.ionicServe = true;
    }
    else {
      if (this.debugMode) {
        console.log("not running ionic serve");
      }
    }
    if (!this.ionicServe) {
      this.badge.clear(); //clear the badge on the launch icon
    }
    this.username = windowService.window.localStorage.getItem('user');
    this.queryPendingSpots();
    // this.queryReservedSpots();
    console.log("entering OFFERpendingReseved page");

    this.num_garages = parseInt(this.windowService.getKey("num_garages","1"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingReservedPage');
  }

  userpage(user){
    this.navCtrl.push(UserInfoPage,user);
  }

  queryPendingSpots() {

    this.pendingSpotsStream$ = new Array();
    this.reservedSpotsStream$ = new Array();

    this.datastream.offerPendingReservedSpotsStream$.map(response => {

      if (this.debugMode) {
        console.log("This is the offerPendingReservedSpotsStream$ datastream provided observable: ");
        console.log(response);
        console.log(response.length);
      }

      return response;
    }).subscribe(filteredSpots => {
      if (this.debugMode) {
        console.log("The spots after mapping the dataStream are: ");
        console.log(filteredSpots);
      }
      setTimeout(() => {

        this.totalPendingSpaces = filteredSpots.totalPendingSpots;
        this.totalReservedSpaces = filteredSpots.totalReservedSpots;
        this.pendingSpotsStream$ = filteredSpots.pendingSpotsRefined;
        this.reservedSpotsStream$ = filteredSpots.reservedSpotsRefined;;

        //this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
        // if (!this.ionicServe) {
        //   this.badge.set(this.pendingSpotsStream$.length);
        // }
      }, 500);
    }, err => {
      console.log("Error with the offers data stream");
      console.log(err);
    })
  }//end of queryPendingSpots


  // queryReservedSpots() {

  //   //this.showLoading("Searching available spots...");

  //   var totalAvailableSpots = 0;
  //   var availableSpotAggregate: any[] = new Array();

  //   var availableSpotsRefined = new Array();

  //   this.reservedSpotsStream$ = new Array();

  //   // set the start date / end date to MSEC format.
  //   this.datastream.offeredSpotsStream$.map(response => {

  //     if (this.debugMode) {
  //       console.log("This is the datastream provided observable: ");
  //       console.log(response);
  //       console.log(response.length);
  //     }

  //     /* First I try to remove all the spots from my stream that 
  //     do not have any offers.  If offers is not undefined then I
  //     push the entire response to a holding variable, aggregate.*/

  //     for (var i = 0; i < response.length; i++) {
  //       if (this.debugMode) {
  //         console.log("Owneruser: ", response[i].owneruser);
  //         console.log("this.username", this.username);
  //       }

  //       if (response[i].offers != undefined && response[i].owneruser == this.username) {
  //         availableSpotAggregate.push(response[i]);
  //       }
  //     }

  //     if (this.debugMode) {
  //       console.log("Available Aggregate.... ");
  //       console.log(availableSpotAggregate);
  //     }

  //     for (var i = 0; i < availableSpotAggregate.length; i++) {

  //       //Object.keys() gets all of the keys for the given input.  In this case, this would create a collection of the offers
  //       //within a given spot.  the .map() function applies a function to all of the values within the collection. (key in this case represents each value)
  //       var tempOfferArray = Object.keys(availableSpotAggregate[i].offers).map(key => availableSpotAggregate[i].offers[key]);

  //       if (this.debugMode) {
  //         console.log("The temp offer Array: ");
  //         console.log(tempOfferArray);
  //         console.log("Temp offer length: " + tempOfferArray.length);
  //       }

  //       if (tempOfferArray.length > 0) {
  //         for (var j = 0; j < tempOfferArray.length; j++) {

  //           if (tempOfferArray[j].offer_status == 3) { //"3" this is the reserved status search parameter

  //             if (this.debugMode) {
  //               console.log("Pushing tempOfferArray[" + j + "]");
  //               console.log(tempOfferArray[j]);
  //             }

  //             totalAvailableSpots++;
  //             availableSpotsRefined.push(new Transaction(tempOfferArray[j]._1_offer_start, tempOfferArray[j]._2_offer_end,
  //               tempOfferArray[j]._3_request_start, tempOfferArray[j]._4_request_end,
  //               tempOfferArray[j].offer_end, tempOfferArray[j].offer_start,
  //               tempOfferArray[j].offer_status, tempOfferArray[j].offer_weight,
  //               tempOfferArray[j].owner, tempOfferArray[j].request_end,
  //               tempOfferArray[j].request_start, tempOfferArray[j].request_user,
  //               tempOfferArray[j].request_weight, tempOfferArray[j].spot));
  //           }
  //         }
  //       }
  //     }

  //     if (this.debugMode) {
  //       console.log("Refined Available Spots: ");
  //       console.log(availableSpotsRefined)
  //     }
  //     return availableSpotsRefined;
  //   }).subscribe(filteredSpots => {
  //     if (this.debugMode) {
  //       console.log("The spots after mapping the dataStream are: ");
  //       console.log(filteredSpots);
  //     }
  //     setTimeout(() => {
  //       //this.loading.dismiss();
  //       this.totalReservedSpaces = totalAvailableSpots;
  //       this.reservedSpotsStream$ = filteredSpots;
  //       //this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.reservedSpotsStream$.length);
  //       // if (!this.ionicServe) {
  //       //   this.badge.set(this.reservedSpotsStream$.length);
  //       // }
  //     }, 500);
  //   }, err => {
  //     console.log("Error with the offers data stream");
  //     console.log(err);
  //   }).complete(() => {
  //     //this.datastream.offeredSpotsStream$.unsubscribe();
  //   });
  // }//end of queryReservedSpots

  showConfirmApprove(spot, i) {
    let confirm = this.alertCtrl.create({
      title: 'Double checking...',
      cssClass: 'custom-alert-check',
      message: 'Are you sure that you wish to lend out your parking spot?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            //delete the item from the page
            this.pendingSpotsStream$.splice(i, 1);
            this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
            if (!this.ionicServe) {
              this.badge.set(this.pendingSpotsStream$.length);
            }

            //Call library function to perform all approve functions
            if (this.debugMode) {
              console.log("spot printed from pendingReserved");
              console.log(spot);
            }
            this.notificationProvider.approve(spot);
            setTimeout(() => {
              this.queryPendingSpots();
              //this.queryReservedSpots();
            }, 1000);
          }
        }
      ]
    });
    confirm.present();
  }

  showConfirmReject(spot, i) {
    let confirm = this.alertCtrl.create({
      title: 'Wait a second...',
      cssClass: 'custom-alert-check',
      message: 'Are you sure that you want to reject this user from using your parking spot?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            //delete the item from the page
            this.pendingSpotsStream$.splice(i, 1);
            this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
            if (!this.ionicServe) {
              this.badge.set(this.pendingSpotsStream$.length);
            }

            //Call library function to perform all deny functions
            if (this.debugMode) {
              console.log(spot);
              console.log("spot printed from pendingReserved");
            }
            this.notificationProvider.deny(spot);
            setTimeout(() => {
              this.queryPendingSpots();
            }, 500);
            // this.queryReservedSpots();
          }
        }
      ]
    });
    confirm.present();
  }

  showCancel(spot, i) {
    if (this.debugMode) {
      console.log('offerPendingReserved ~ 388 ~ owner is trying to cancel spot: ', spot);
    }
    let confirm = this.alertCtrl.create({
      title: 'Cancel this Reservation',
      cssClass: 'custom-alert-danger',
      message: 'We understand you may need your spot, and we thank you for making your spot available to others.  Sharing is caring,' +
      ' and we just want to make sure you <emphasis>really</emphasis> can\'t lend your spot out before you cancel this reservation...',
      buttons: [
        {
          text: 'On second thought, keep this reservation active',
          handler: () => {
            confirm.dismiss();
          }
        },
        {
          text: 'I really need my spot.  CANCEL',
          handler: () => {
            console.log("Cancelled");

            let resHandler = new ReservationHandler(this.alertCtrl, this.database, this.auth);

            resHandler.ownerCancel(spot).then(result => {
              if (this.debugMode) {
                console.log('Successfully returned from the owner cancellation');
              }

              let start: String = moment(spot._3_request_start).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
              let end: String = moment(spot._4_request_end).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
              
              let notificationMessage = spot.request_user + 'You had a spot, but the owner needed it after all. \n' + spot.owner + ' cancelled your reservation on \n'
              start + ' \n To: \n ' + end + '\n We are sorry for the inconvenience.  Please search our available spots and request again.';

              let htmlMessage = `<p>You had a spot, but the owner needed it after all. :( </p>` + 
                              `<p>` + spot.owner + ` cancelled your reservation on </p>` +
                              `<p>` + start + `</p><p> To: <p>` + end + `</p>`

              var payload = { 
                "spot": spot,
                "htmlMessage": htmlMessage
              }

              this.notificationProvider.sendNotification(spot.request_user, notificationMessage, "alert", payload);
              setTimeout(() => {
                this.queryPendingSpots();
              }, 500);
            }).catch(err => {
              if (this.debugMode) {
                console.log('Something went wrong when the owner tried to cancel.  Err code: ', err);
              }

              let alert = this.alertCtrl.create({
                title: '>_^',
                cssClass: 'custom-alert-danger',
                message: 'I failed to complete the cancellation.  Please try again.',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      console.log('Alert dismissed.');
                      alert.dismiss();
                    }
                  }]
              });
              alert.present();;
            })
            // //delete the item from the page
            // this.pendingSpotsStream$.splice(i, 1);
            // this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
            // this.badge.set(this.pendingSpotsStream$.length);

            // //Call library function to perform all deny functions
            // if (this.debugMode) {
            //   console.log(spot);
            //   console.log("spot printed from pendingReserved");
            // }
            // this.notificationProvider.deny(spot);

          }
        }
      ]
    });
    confirm.present();
  }
}
