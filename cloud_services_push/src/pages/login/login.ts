import { Component } from '@angular/core';
import { Platform, NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { InvitationPage } from '../invitation/invitation';
import { HomePage } from '../home/home';
import { PasswordResetPage } from '../passwordReset/passwordReset';
//import { MessagingPage } from '../messaging/messaging';
import { Auth, User } from '@ionic/cloud-angular'; //UserDetails, IDetailedError, Push, PushToken
import { WindowService } from '../../providers/window-service';
// import { ConversationsPage } from '../conversations/conversations'; // MAY NOT BE USED ANYMORE - REMOVE FROM THIS PAGE?
import { LandingPage } from '../landing/landing';
import { Landing2Page } from '../landing2/landing2';
import { GenericDatabase } from '../../providers/generic-database';
import { DebugMessage } from '../../components/library';
import * as moment from 'moment'
// import { Observable } from 'rxjs/Observable';
import { DatastreamService } from '../../providers/datastream-service';
import { NotificationProvider } from '../../providers/notification-provider';
import { AppVersion } from '@ionic-native/app-version';

// the login, signup and auth-service provider were created using this tutorial: https://devdactic.com/login-ionic-2/ so check it out if you're confused -BLT

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  loading: Loading;
  registerCredentials = { email: '', password: '', rememberMe: "false" };
  badVersion: boolean = false;
  versionNumber: any;
  minVersion: any[] = ["00", "03", "00"];
  html_rememberMe: boolean = false;
  private debugMode: boolean = false;


  constructor(private navCtrl: NavController, private authprovider: AuthService,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController,
    public auth: Auth, public user: User,
    private windowService: WindowService,
    public database: GenericDatabase, private datastreams: DatastreamService,
    private notificationProvider: NotificationProvider,
    private app: AppVersion, private platform: Platform) {

    var ionicServe = false;
    if (!this.platform.is("cordova")) {
      if (this.debugMode) {
        console.log("running ionic serve");
      }
      ionicServe = true;
    }

    console.log("entering login page");

    if (!ionicServe) {
      this.compareVersions();
    }
  }

  ionViewDidLoad() {
    var remember = this.windowService.getKey('remember', "false");
    if (remember == "true") {
      this.registerCredentials.email = this.windowService.getKey('user', '');
      this.registerCredentials.password = this.windowService.getKey('password', '');
      this.registerCredentials.rememberMe = remember;
      this.html_rememberMe = true;
    } else {
      this.windowService.setKey('user', '');
      this.windowService.setKey('password', '');
      this.registerCredentials.email = "",
        this.registerCredentials.password = ""
    }
    if (this.debugMode) {
      console.log(this.windowService.window);
      console.log(this.registerCredentials);
    }
  }

  public gotoInvitation() {
    // Added this to reset user/password combination if "remember me" is set.
    // this.windowService.setKey('user', '');
    // this.windowService.setKey('password', '');
    // this.windowService.setKey('remember', '');
    // this.registerCredentials.email = "";
    // this.registerCredentials.password = "";
    // this.registerCredentials.rememberMe = false;
    this.navCtrl.push(InvitationPage);
  }

  public login() {

    // needs: 
    // import { DebugMessage } from '../../components/library';
    // import { GenericDatabase } from '../../providers/generic-database';
    // DebugMessage: {user, page, info, datetime (in local time), comment}
    let debug = new DebugMessage(this.registerCredentials.email, "signin", JSON.parse(JSON.stringify(this.registerCredentials)), moment().toLocaleString(), "successful login");
    if (debug.debugOnOff) {
      this.database.update_DB_childByParent("/debug/", this.registerCredentials.email.replace(/\./g, ","), moment().unix().toString(), debug);
    }
    this.updateStorage();
    this.showLoading();
    var access = false;
    // var cordova: any;
    // console.log("*******CORDOVA PLUGINS******")
    // console.log(cordova.plugins);

    this.auth.login('basic', this.registerCredentials)
      .then((x) => {
        if (this.debugMode) {
          console.log("the auth login returned", x);
        }

        this.authprovider.storeUserInfo(this.user.details.email);

        this.notificationProvider.register(); //this registers the push notification

        access = true;
        console.log(x);
        setTimeout(() => {
          this.loading.dismiss();
          console.log(this.auth);
          this.navCtrl.setRoot(Landing2Page, this.auth);
        })
      },
      (err) => {
        console.log(err);
        access = false;
        let errors = '';
        if (err.message === 'UNPROCESSABLE ENTITY') errors += 'Email isn\'t valid.<br/>';
        if (err.message === 'UNAUTHORIZED') errors += 'Password is required.<br/>';
        this.showError("Access Denied");
      });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    //let access = (credentials.password === "pass" && credentials.email === "email");
    //this.currentUser = new User_local('Simon', 'saimon@devdactic.com');

    this.windowService.setKey('user', this.registerCredentials.email);
    this.windowService.setKey('password', this.registerCredentials.password);
    this.windowService.setKey('remember', this.registerCredentials.rememberMe);

    //Look up user info and put it into windowService
    let tempPromise = this.database.queryDB_single_item_as_Promise("users", this.registerCredentials.email.replace(/\./g, ","));
    tempPromise.then(onNext => {

      let reply = onNext;

      if (this.debugMode) {
        console.log("login.ts line ~ 144 queryDB_Single_promise result: ", reply)
      }

      for (var i = 0; i < onNext.length; i++) {
        if (this.debugMode) {
          console.log(onNext[i].$key, onNext[i].$value);
        }
        this.windowService.setKey(onNext[i].$key, onNext[i].$value);
      }

      if (this.debugMode) {
        console.log("Check out windoservice");
        // THIS ALLOWS US TO PRINT THE WINDOW OBJECT
        // THERE YOU CAN DRILL INTO LOCAL STORAGE
        console.log(this.windowService);
      }

      var tempPromise2 = this.database.queryDB_single_item_as_Promise("buildings/" + this.windowService.getKey("building", "none"), "building_info");
      tempPromise2.then(onNext => {

        let reply = onNext;

        for (var i = 0; i < onNext.length; i++) {
          if (this.debugMode) {
            console.log(onNext[i].$key, onNext[i].$value);
          }
          this.windowService.setKey(onNext[i].$key, onNext[i].$value);
        }

        if (this.debugMode) {
          console.log("Check out windoservice");
          // THIS ALLOWS US TO PRINT THE WINDOW OBJECT
          // THERE YOU CAN DRILL INTO LOCAL STORAGE
          console.log(this.windowService);
        }
      });
    }).catch(onError => {
      console.log("onError: ", onError)
    });
    // //Updating the login query item.  This grabs the car color (and all other registration info) to be passed to local storage.

    // //look up building and put info into windowservice
    // tempPromise = this.database.queryDB_single_item_as_Promise("buildings/" + this.windowService.getKey("building", "none"), "building_info");
    // tempPromise.then(onNext => {

    //   let reply = onNext;

    //   if (this.debugMode) {
    //     console.log("login.ts line ~ 170 queryDB_Single_promise result: ", reply)
    //   }

    //   for (var i = 0; i < onNext.length; i++) {
    //     if (this.debugMode) {
    //       console.log(onNext[i].$key, onNext[i].$value);
    //     }
    //     this.windowService.setKey(onNext[i].$key, onNext[i].$value);
    //   }

    //   if (this.debugMode) {
    //     console.log("Check out windoservice");
    //     // THIS ALLOWS US TO PRINT THE WINDOW OBJECT
    //     // THERE YOU CAN DRILL INTO LOCAL STORAGE
    //     console.log(this.windowService);
    //   }
    // }).catch(onError => {
    //   console.log("onError: ", onError)
    // });

  }

  updateStorage() {
    if (this.html_rememberMe) {
      this.registerCredentials.rememberMe = "true";
    } else {
      this.registerCredentials.rememberMe = "false";
    }
    if (this.html_rememberMe) {
      this.windowService.setKey('user', this.registerCredentials.email.toLowerCase());
      this.windowService.setKey('password', this.registerCredentials.password);
      this.windowService.setKey('remember', "true");
    } else {
      this.windowService.setKey('user', '');
      this.windowService.setKey('password', '');
      this.windowService.setKey('remember', 'false');
    }
    if (this.debugMode) {
      console.log(this.windowService.window);
    }

  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(text) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      cssClass: 'custom-alert-danger',
      title: 'Fail',
      message: `<p>` + text + `</p>`,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  goToDevMain() {
    this.navCtrl.push(HomePage);
  }

  forgotPassword() {
    this.navCtrl.push(PasswordResetPage);
  }

  compareVersions() {
    console.log("entering compareVersions");

    this.database.queryDB_single_item_as_Promise("/admin/", "").then(x => {

      this.minVersion = x[0].$value.split(".");
      console.log('The min version is: ', this.minVersion);

      this.app.getVersionNumber().then((versionNumber) => {

        this.versionNumber = versionNumber.split(".");
        console.log('Your current version is: ', versionNumber)
        this.compareAndSet(this.versionNumber, this.minVersion);

      }).catch((err) => {

        console.log(err);

      })
    }).catch((err) => {

      console.log(err);

    })

  }

  compareAndSet(current, min) {
    console.log("compareAndSet-current and min");
    console.log(current);
    console.log(min);

    // compare them:
    //this always assumes that the lowest number iterates before the others.
    //so, 1.23.00 is less than version 1.23.01
    if (parseInt(current[0]) >= parseInt(min[0])) {
      if (parseInt(current[1]) >= parseInt(min[1])) {
        if (parseInt(current[2]) >= parseInt(min[2])) {
          this.badVersion = false;

        } else {
          this.badVersion = true;
        }
      } else {
        this.badVersion = true;
      }
    } else {
      this.badVersion = true;
    }
  }





}