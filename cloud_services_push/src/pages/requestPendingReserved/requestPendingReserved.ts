import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
//import { Auth, User, UserDetails, IDetailedError, Push, PushToken } from '@ionic/cloud-angular';
import { WindowService } from '../../providers/window-service';
import { AuthService } from '../../providers/auth-service';
//import { ManagementPage } from '../management/management';
//import { OfferPage } from '../offer/offer';
//import { RequestPage } from '../request/request';
//import { SettingsPage } from '../settings/settings';
//import { LoginPage } from '../login/login';
import { GenericDatabase } from '../../providers/generic-database';
import { DatastreamService } from '../../providers/datastream-service';
import { AlertController } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification-provider';
import { ReservationHandler } from '../../components/reservationhandler';
// import * as moment from 'moment';
import { Badge } from '@ionic-native/badge';
import * as moment from 'moment';
import { UserInfoPage } from '../userinfo/userinfo'
// import { Transaction } from '../../components/library';

/*
  Generated class for the Landing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-requestPendingReseved',
  templateUrl: 'requestPendingReserved.html'
})
export class RequestPendingReservedPage {

  username: String;
  private debugMode: boolean = true;
  private totalPendingSpaces: number = 0;
  private totalReservedSpaces: number = 0;

  private pendingSpotsStream$: any[]
  private reservedSpotsStream$: any[]
  private ionicServe: boolean = false;
  private num_garages: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private windowService: WindowService,
    private datastream: DatastreamService, private alertCtrl: AlertController, private notificationProvider: NotificationProvider,
    private database: GenericDatabase, private badge: Badge, private platform: Platform, private auth: AuthService) {

    //INITIALIZATION//
    if (!this.platform.is("cordova")) {
      if (this.debugMode) {
        console.log("running ionic serve");
      }
      this.ionicServe = true;
    }
    else {
      if (this.debugMode) {
        console.log("not running ionic serve");
      }
    }
    if (!this.ionicServe) {
      this.badge.clear(); //clear the badge on the launch icon
    }
    this.username = windowService.window.localStorage.getItem('user');
    this.queryPendingSpots();
    //this.queryReservedSpots();
    console.log("entering REQUESTpendingReseved page");
    this.num_garages = parseInt(this.windowService.getKey("num_garages","1"));
  }
  //END INITIALIZATION//

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingReservedPage');
  }

  userpage(user){
    this.navCtrl.push(UserInfoPage,user);
  }

  queryPendingSpots() {

    this.pendingSpotsStream$ = new Array();
    this.reservedSpotsStream$ = new Array();

    this.datastream.requestPendingReservedSpotsStream$.map(response => {
      if (this.debugMode) {
        console.log("the requestPendingReserved page requestPendingReservedSpotStream$ provided observable: ", response.length);
      }
      return response;
    }).subscribe(filteredSpots => {
      if (this.debugMode) {
        console.log("The spots after mapping the dataStream are: ");
        console.log(filteredSpots);
      }
      setTimeout(() => {
        //this.loading.dismiss();

        this.totalPendingSpaces = filteredSpots.totalPendingSpots;
        this.totalReservedSpaces = filteredSpots.totalReservedSpots;
        this.pendingSpotsStream$ = filteredSpots.pendingSpotsRefined;
        this.reservedSpotsStream$ = filteredSpots.reservedSpotsRefined;;

        //this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
        // if (!this.ionicServe) {
        //   this.badge.set(this.pendingSpotsStream$.length);
        // }
      }, 500);
    }, err => {
      console.log("Error with the offers data stream");
      console.log(err);
    })
  }//end of queryPendingSpots





  // queryReservedSpots() {

  //   //this.showLoading("Searching available spots...");

  //   var totalAvailableSpots = 0;
  //   var availableSpotAggregate: any[] = new Array();
  //   var availableSpotsRefined: {
  //     _1_offer_start: String,
  //     _2_offer_end: String,
  //     _3_request_start: String,
  //     _4_request_end: String,
  //     offer_end: number,
  //     offer_start: number,
  //     offer_status: number,
  //     offer_weight: number,
  //     owner: String,
  //     request_end: number,
  //     request_start: number,
  //     request_user: String,
  //     request_weight: number,
  //     spot: String
  //   }[] = new Array();

  //   this.reservedSpotsStream$ = new Array();

  //   // set the start date / end date to MSEC format.
  //   this.datastream.offeredSpotsStream$.map(response => {

  //     if (this.debugMode) {
  //       // console.log("This is the datastream provided observable: ");
  //       // console.log(response);
  //       // console.log(response.length);
  //     }

  //     /* First I try to remove all the spots from my stream that 
  //     do not have any offers.  If offers is not undefined then I
  //     push the entire response to a holding variable, aggregate.*/

  //     for (var i = 0; i < response.length; i++) {

  //       if (response[i].offers != undefined) {
  //         availableSpotAggregate.push(response[i]);
  //       }
  //     }

  //     if (this.debugMode) {
  //       // console.log("Available Aggregate.... ");
  //       // console.log(availableSpotAggregate);
  //     }

  //     for (var i = 0; i < availableSpotAggregate.length; i++) {

  //       //Object.keys() gets all of the keys for the given input.  In this case, this would create a collection of the offers
  //       //within a given spot.  the .map() function applies a function to all of the values within the collection. (key in this case represents each value)
  //       var tempOfferArray = Object.keys(availableSpotAggregate[i].offers).map(key => availableSpotAggregate[i].offers[key]);

  //       if (this.debugMode) {
  //         // console.log("The temp offer Array: ");
  //         // console.log(tempOfferArray);
  //         // console.log("Temp offer length: " + tempOfferArray.length);
  //       }

  //       if (tempOfferArray.length > 0) {
  //         for (var j = 0; j < tempOfferArray.length; j++) {
  //           if (this.debugMode) {
  //             // console.log("start and stop values for loop");
  //             // console.log(tempOfferArray[j].offer_start);
  //             // //console.log(this.startingMsec);
  //             // console.log(tempOfferArray[j].offer_end);
  //             // //console.log(this.endingMsec);

  //           }
  //           if (tempOfferArray[j].offer_status == 3 && tempOfferArray[j].request_user == this.username) { //"3" this is the reserved status search parameter

  //             if (this.debugMode) {
  //               // console.log("Pushing tempOfferArray[" + j + "]");
  //               // console.log(tempOfferArray[j]);
  //             }



  //             totalAvailableSpots++;
  //             availableSpotsRefined.push({
  //               spot: availableSpotAggregate[i].$key,
  //               _1_offer_start: moment.utc(tempOfferArray[j].offer_start).local().toString(),
  //               _2_offer_end: moment.utc(tempOfferArray[j].offer_end).local().toString(),
  //               _3_request_start: moment.utc(tempOfferArray[j].request_start).local().toString(),
  //               _4_request_end: moment.utc(tempOfferArray[j].request_end).local().toString(),
  //               offer_weight: tempOfferArray[j].offer_weight,
  //               request_weight: tempOfferArray[j].request_weight,
  //               offer_status: tempOfferArray[j].offer_status,
  //               owner: availableSpotAggregate[i].owneruser,
  //               request_start: tempOfferArray[j].request_start,
  //               request_end: tempOfferArray[j].request_end,
  //               request_user: tempOfferArray[j].request_user,
  //               offer_start: tempOfferArray[j].offer_start,
  //               offer_end: tempOfferArray[j].offer_end
  //             });
  //           }
  //         }
  //       }
  //     }

  //     if (this.debugMode) {
  //       console.log("Refined Available Spots: ");
  //       console.log(availableSpotsRefined)
  //     }
  //     return availableSpotsRefined;
  //   }).subscribe(filteredSpots => {
  //     if (this.debugMode) {
  //       console.log("The spots after mapping the dataStream are: ");
  //       console.log(filteredSpots);
  //     }
  //     setTimeout(() => {
  //       //this.loading.dismiss();
  //       this.totalReservedSpaces = totalAvailableSpots;
  //       this.reservedSpotsStream$ = filteredSpots;
  //       //this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.reservedSpotsStream$.length);
  //       // if (!this.ionicServe) {
  //       //   this.badge.set(this.reservedSpotsStream$.length);
  //       // }
  //     }, 500);
  //   }, err => {
  //     console.log("Error with the offers data stream");
  //     console.log(err);
  //   })
  // }//end of queryReservedSpots



  /**
   * Confirmation of cancellation for a PENDING spot.
   * 
   * @param spot 
   * @param i 
   */

  showConfirmPendCancel(spot, i) {
    let confirm = this.alertCtrl.create({
      title: 'Double checking...',
      message: 'Are you sure that you want to cancel your pending reservation?',
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            //delete the item from the page
            this.pendingSpotsStream$.splice(i, 1);

            //Call library function to perform all cancelation functions
            if (this.debugMode) {
              console.log("spot printed from pendingReserved");
              console.log(spot)
              console.log(spot.spot);
              console.log(spot.offer_start);
            }
            this.database.update_DB_childByParent('/buildings/' + spot.building + '/' + spot.garage + "/spots/" + spot.spot + "/offers/", "offer_" + spot.offer_start, "offer_status", 1);

            setTimeout(() => {
              this.queryPendingSpots();
            }, 500);
          }
        }
      ]
    });
    confirm.present();
  }


  /**
   * Confirmation handler for cancellation of
   * a RESERVED spot.
   * 
   * @param spot 
   * @param i 
   */
  showConfirmResCancel(spot, i) {
    let confirm = this.alertCtrl.create({
      title: 'Wait a second...',
      message: 'Are you sure that you want to cancel your existing reservation?',
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            //delete the item from the page
            this.pendingSpotsStream$.splice(i, 1);

            //Call library function to perform all deny functions
            if (this.debugMode) {
              console.log("Renter cancellation request confirmed for the spot: ", spot, + " requestPendingReserved.ts ~ line 408 ");
            }

            let resHandler = new ReservationHandler(this.alertCtrl, this.database, this.auth);

            resHandler.renterCancel(spot).then(result => {
              if (this.debugMode) {
                console.log('Successfully returned from the renter cancellation');
              }

              //let start: String = moment.utc(this.startingMsec).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
              //let end: String = moment.utc(this.endingMsec).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();

              let start: String = moment(spot._3_request_start).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
              let end: String = moment(spot._4_request_end).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();

              let notificationMessage = this.username + ' cancelled their reservation to use your spot from \n' +
                start + ' \n To: \n' + end + '\nWe have remade your spot available for rental' +
                ' during the cancelled time.  Please check your offers for more information.'

              let htmlMessage = `<p>` + this.username + ` cancelled their reservation to use your spot from: `  +
                                `<p>` + start + ` </p><p>To:</p> <p>` + end + `</p>
                                <p> We have made your spot available for rental during the cancelled time.</p>
                                <p>Please check your offers for more information.</p>`

              var payload = { 
                "spot": spot,
                "htmlMessage": htmlMessage
              }

              this.notificationProvider.sendNotification(spot.owner, notificationMessage, "notify", payload);

              setTimeout(() => {
                this.queryPendingSpots();
              }, 500);


            }).catch(err => {
              if (this.debugMode) {
                console.log('Something went wrong when the renter tried to cancel.  Err code: ', err);
              }
              let alert = this.alertCtrl.create({
                title: '>_^',
                message: 'I failed to complete the cancellation.  Please try again.',
                cssClass: 'custom-alert-danger',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      console.log('Alert dismissed.');
                      alert.dismiss();
                    }
                  }]
              });
              alert.present();;
            });

            //this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers/", "offer_" + spot.offer_start, "offer_status", 1 );

            // this.queryReservedSpots();

          }
        }
      ]
    });
    confirm.present();
  }

}
