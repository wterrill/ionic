import { Component } from '@angular/core';
import { Platform, NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { PasswordResetPage } from '../passwordReset/passwordReset';
import { Auth, User } from '@ionic/cloud-angular'; 
import { WindowService } from '../../providers/window-service';
import { LandingPage } from '../landing/landing';
import { GenericDatabase } from '../../providers/generic-database';
import { DebugMessage } from '../../components/library';
import * as moment from 'moment'
import { DatastreamService } from '../../providers/datastream-service';
import { NotificationProvider } from '../../providers/notification-provider';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-invitation',
  templateUrl: 'invitation.html'
})

export class InvitationPage {
  private invitationCredentials = { invitationCode: '', building: '', firstname: '', lastname: '', spot: '', unit: '', email: '' , used: false};
  private debugMode: boolean = true;


  constructor(private navCtrl: NavController, private authprovider: AuthService,
    private alertCtrl: AlertController, 
    public database: GenericDatabase,
    private datastreams: DatastreamService
  ) {
    console.log("entering invitation page");

  }


  public invite() {

    this.database.queryDB_single_item_as_Promise("invitations/", this.invitationCredentials.invitationCode).then((result) => {
      if (result == "") {
        this.not_there_alert();
      }
      else {
        console.log(result)
        for (var i = 0; i < result.length; i++) {
          this.invitationCredentials[result[i].$key]  = result[i].$value;
        }
        if(!this.invitationCredentials.used){
          this.database.update_DB_childByParent("invitations/", this.invitationCredentials.invitationCode, "used", true);
          this.navCtrl.push(SignupPage, {passed_from_Invitation: this.invitationCredentials});
        } else {
          this.not_there_alert();
        }

      }
    }).catch((error) => {
      alert("it's not there");
    });
  }

  not_there_alert() {
    let alert = this.alertCtrl.create({
      title: "Invalid Invitation Code:",
      subTitle: '',
      cssClass: 'custom-alert-danger',
      message: `<p>We're sorry, the invitation code that you entered doesn't exist...</p>
      <p>...or, it might already have been redeemed.</p>
      <img src="http://4.bp.blogspot.com/-SiCjX8WRa_8/VCt78yHRSwI/AAAAAAAAT5U/aNksu356nLI/s1600/sad-sun.png" style="width: 50%; height: 50%"/>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  
}