import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//Added by Will
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { AuthService } from '../providers/auth-service';
import { ImagemapPage } from '../pages/imagemap/imagemap';
import { SvgmapPage } from '../pages/svgmap/svgmap';
import { SvgzoomPage } from '../pages/svgzoom/svgzoom';
import { ImgMapComponent } from 'ng2-img-map';
import { MapPage } from '../pages/map/map';
import { AppVersion } from '@ionic-native/app-version';
import { QueryPage } from '../pages/query/query';
import { SpotExistsValidator } from '../validators/spotExistsValidator';
//import { IonicImageViewerModule } from 'ionic-img-viewer';
import { GalleryModal } from 'ionic-gallery-modal';
import { ZoomableImage } from 'ionic-gallery-modal';
//import { MapzoomPage } from '../pages/mapzoom/mapzoom';
import { PrettyPrint } from '../pages/message-modal/newlinePipe';
import { GenericDatabase } from '../providers/generic-database';
import { Badge } from '@ionic-native/badge';
// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
//Added by Leif
import { MessagesDatabase } from '../providers/messages-database';
import { ConversationsDatabase } from '../providers/conversations-database';
import { MessagingPage } from '../pages/messaging/messaging';
import { MessageModalPage } from '../pages/message-modal/message-modal';
import { ConversationsPage } from '../pages/conversations/conversations';
import { WindowService } from '../providers/window-service';
import { LandingPage } from '../pages/landing/landing';
import { Landing2Page } from '../pages/landing2/landing2';
import { OfferPage } from '../pages/offer/offer';
import { RequestPage } from '../pages/request/request';
import { ManagementPage } from '../pages/management/management';
import { SettingsPage } from '../pages/settings/settings';
import { OfferPendingReservedPage } from '../pages/offerPendingReserved/offerPendingReserved';
import { RequestPendingReservedPage } from '../pages/requestPendingReserved/requestPendingReserved';
import { DatastreamService } from '../providers/datastream-service';
import { NotificationProvider } from '../providers/notification-provider';
import { PasswordResetPage } from '../pages/passwordReset/passwordReset';
import { InvitationPage } from '../pages/invitation/invitation';
import { UserInfoPage } from '../pages/userinfo/userinfo';

// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyAdn1iBQFMr4oRJVA6YXUDfDo1umxeFrEQ",
  authDomain: "neighborly-577c2.firebaseapp.com",
  databaseURL: "https://neighborly-577c2.firebaseio.com",
  projectId: "neighborly-577c2",
  storageBucket: "neighborly-577c2.appspot.com",
  messagingSenderId: "100456635899"
};

/*
I think this is old code?  Otherwise how would our notifications ever work?

App ID chould be a CONST but I don't know if Sender ID shoudl be... isn't that tied to your device token?
Just trying to wrap my head back around these things....
*/
const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '9a7f489c',
  },
  'push': {
    'sender_id': '100456635899',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    MessagingPage,
    MessageModalPage,
    LoginPage,
    ConversationsPage,
    MapPage,
    ImagemapPage,
    SvgmapPage,
    SvgzoomPage,
    ImgMapComponent,
    PrettyPrint,
    LandingPage,
    Landing2Page,
    OfferPage,
    RequestPage,
    ManagementPage,
    SettingsPage,
    OfferPendingReservedPage,
    RequestPendingReservedPage,
    GalleryModal,
    ZoomableImage,
    QueryPage,
    PasswordResetPage,
    InvitationPage,
    UserInfoPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    MessagingPage,
    MessageModalPage,
    LoginPage,
    ConversationsPage,
    MapPage,
    SvgzoomPage,
    ImagemapPage,
    SvgmapPage,
    LandingPage,
    Landing2Page,
    OfferPage,
    RequestPage,
    ManagementPage,
    SettingsPage,
    OfferPendingReservedPage,
    RequestPendingReservedPage,
    GalleryModal,
    QueryPage,
    PasswordResetPage,
    InvitationPage,
    UserInfoPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler },  
                AuthService, 
                MessagesDatabase, 
                ConversationsDatabase, 
                WindowService, 
                GenericDatabase, 
                DatastreamService, 
                NotificationProvider,
                Badge,
                AppVersion,
                SpotExistsValidator
                //PhotoViewer
                ]
})
export class AppModule { }
